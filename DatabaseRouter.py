class LeapRouter(object):
    """A router to control all database operations on models in
    the leap application"""

    def db_for_read(self, model, **hints):
        "Point all operations on leap models to 'life_equity'"
        if model._meta.app_label == 'leap':
            return 'life_equity'
        return 'default'

    def db_for_write(self, model, **hints):
        #import pdb; pdb.set_trace()
        "Point all operations on leap models to 'life_equity'"
        if model._meta.app_label == 'leap':
            return None #'life_equity'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a model in leap is involved"
        if obj1._meta.app_label == 'leap' or obj2._meta.app_label == 'leap':
            return True
        return True

    def allow_syncdb(self, db, model):
        "Make sure the leap app only appears on the 'life_equity' db"
        if db == 'life_equity':
            return model._meta.app_label == 'leap'
        elif model._meta.app_label == 'leap':
            return False
        return None
