$(document).ready(function() {
    $(".policy_detail_overlay").click(function(){
        return show_overlay("/leap/policy_detail/table/", $(this).attr('rel'), 'Policy Details');
    });

    $(".insured_detail_overlay").click(function(){
        return show_overlay("/leap/insured_detail/table/", $(this).attr('rel'), 'Insured Details');
    });

    $("tr.life_expectancy_overlay").dblclick(function(){
        return show_overlay("/leap/life_expectancy_table/", $(this).attr('rel'), 'Life Expectancy Details');
    });

    $("tr.grace_event_overlay").dblclick(function(){
        return show_overlay("/leap/grace_event_table/", $(this).attr('rel'), 'Grace Event Details');
    });

    $("tr.maturity_claim_overlay").dblclick(function(){
        return show_overlay("/leap/maturity_claim_table/", $(this).attr('rel'), 'Maturity Claim Details');
    });

    $("tr.premium_overlay").dblclick(function(){
        return show_overlay("/leap/premium_table/", $(this).attr('rel'), 'Premium Details');
    });

    $("tr.insured_events_overlay").dblclick(function(){
        return show_overlay("/leap/insured_events_table/", $(this).attr('rel'), 'Insured Event Details');
    });

    $(".policy_event_overlay").dblclick(function(){
        return show_overlay("/leap/policy_event_table/", $(this).attr('rel'), 'Policy Event Details');
    });
    $("tr.insurance_company_overlay").dblclick(function(){
        return show_overlay("/leap/insurance_company_table/", $(this).attr('rel'), 'Insurance Company Details');
    });

    $('.panel .collapse').click(function(){
        if ($(this).closest('.panel').hasClass('collapsed')){
            var restoreHeight = $(this).attr('id');
            
            $(this).closest('.panel').animate({height:restoreHeight+'px'}, function() {   
                $(this).removeClass('collapsed');
            });
            
        }else{
            var currentHeight = $(this).closest('.panel').height();
            
            $(this).attr('id', currentHeight);
            $(this).closest('.panel').addClass('collapsed').animate({height:'45px'}, function(){        });
        }
    }); 

    function pieHover(event, pos, obj){
        if (!obj) return;
        percent = parseFloat(obj.series.percent).toFixed(2);

        $("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
    }


    function pieClick(event, pos, obj){
        if (!obj) return;

        percent = parseFloat(obj.series.percent).toFixed(2);
        alert(''+obj.series.label+': '+percent+'%');
    }


    function log(message){
        //console.log(message);
    }
	function init_sideNavigation(){
		
		$("#navigation > li > a").click(function(){
			var parent = $(this).closest('li');
			
			if ($('ul',parent).size()){
			
				if ($(parent).hasClass('active')){
					$('ul',parent).slideUp('fast',function(){
						$(parent).removeClass('active');
					});
				}else{
					$('ul',parent).slideDown('fast');
					$(parent).addClass('active');
				}
				
				return false;
			}		
		});

	}

    init_sideNavigation();
        jQuery.fn.dataTableExt.oSort['currency-asc'] = function(a,b) {
            /* Remove any formatting */
            var x = a == "-" ? 0 : a.replace( /[^\d\-\.]/g, "" );
            var y = b == "-" ? 0 : b.replace( /[^\d\-\.]/g, "" );
            
            /* Parse and return */
            x = parseFloat( x );
            y = parseFloat( y );
            return x - y;
        };
        
        jQuery.fn.dataTableExt.oSort['currency-desc'] = function(a,b) {
            var x = a == "-" ? 0 : a.replace( /[^\d\-\.]/g, "" );
            var y = b == "-" ? 0 : b.replace( /[^\d\-\.]/g, "" );
            
            x = parseFloat( x );
            y = parseFloat( y );
            return y - x;
        };

});
    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num)) num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + '$' + num + '.' + cents);
    }
    function show_overlay(url, id, title){
        var NewDialog = $('<div></div>');
        NewDialog.load(url + id + "/", function() {
            var container = $(this);
                NewDialog.dialog({
                    height: 500,
                    title: title,
                    width: 400
                });
            container.dialog({
                modal: true
            })
        });
        return false;
    }
