import os.path
import posixpath
import sys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
#sys.path.append(PROJECT_ROOT)
#sys.path.append(PROJECT_ROOT + '/apps')
DEBUG = True
TEMPLATE_DEBUG = DEBUG

# tells Pinax to serve media through the staticfiles app.
SERVE_MEDIA = DEBUG

# django-compressor is turned off by default due to deployment overhead for
# most users. See <URL> for more information
COMPRESS = False

INTERNAL_IPS = [
    "127.0.0.1",
    "76.188.47.154",
    "69.40.159.2",
    "66.61.8.24",
]

ADMINS = [
    # ("Your Name", "your_email@domain.com"),
     ("Rob Tucker", "rtucker@cybrdev.com"),
]

MANAGERS = ADMINS



# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "site_media", "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = "/site_media/media/"

# Absolute path to the directory that holds static files like app media.
# Example: "/home/media/media.lawrence.com/apps/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, "site_media", "static")

# URL that handles the static files like app media.
# Example: "http://media.lawrence.com"
#STATIC_URL = "/site_media/static/"
STATIC_URL = "https://s3.amazonaws.com/le-static-media/"

# Additional directories which hold static files
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "static"),
]

STATICFILES_FINDERS = [
    "staticfiles.finders.FileSystemFinder",
    "staticfiles.finders.AppDirectoriesFinder",
    "staticfiles.finders.LegacyAppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

AWS_ACCESS_KEY_ID = 'AKIAJEBP55Z5WZT2BJ6Q'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'le-static-media'
# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = posixpath.join(STATIC_URL, "admin/")

# Subdirectory of COMPRESS_ROOT to store the cached media files in
COMPRESS_OUTPUT_DIR = "cache"

# Make this unique, and don't share it with anybody.

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = [
    #"django.template.loaders.filesystem.load_template_source",
    #"django.template.loaders.app_directories.load_template_source",
    'django.template.loaders.filesystem.Loader', 
    'django.template.loaders.app_directories.Loader', 
]

MIDDLEWARE_CLASSES = [
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "pagination.middleware.PaginationMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "urls"

TEMPLATE_DIRS = [
    os.path.join(PROJECT_ROOT, "templates"),
]

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    
    "staticfiles.context_processors.static",
    
    
    "notification.context_processors.notification",
    "announcements.context_processors.site_wide_announcements",
]

INSTALLED_APPS = [
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.humanize",
    "south",
    "leap",
    "local_app",
    "storages",
    
    #"custom_template_tags",
    
    # theme
    
    # external
    "notification", # must be first
    "staticfiles",
    "compressor",
    "debug_toolbar",
    "mailer",
    "timezones",
    "emailconfirmation",
    "announcements",
    "pagination",
    
    # Pinax
    
    # project
    #"about",
    "gunicorn",
]

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

EMAIL_BACKEND = "mailer.backend.DbBackend"

ABSOLUTE_URL_OVERRIDES = {
    "auth.user": lambda o: "/profiles/profile/%s/" % o.username,
}

AUTH_PROFILE_MODULE = "profiles.Profile"
NOTIFICATION_LANGUAGE_MODULE = "account.Account"


AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

LOGIN_URL = "/login/" # @@@ any way this can be a url name?
LOGIN_REDIRECT_URLNAME = "what_next"
LOGOUT_REDIRECT_URLNAME = "home"

EMAIL_CONFIRMATION_DAYS = 2
EMAIL_DEBUG = DEBUG

DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
}

# local_settings.py can be used to override environment-specific settings
# like database and email that differ between development and production.
ADMINS = [
    # ("Your Name", "your_email@domain.com"),
]

MANAGERS = ADMINS

DATABASES = {
    "default": {
        #"ENGINE": "django.db.backends.postgresql_psycopg2", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.mysql", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".       
        "NAME": "lifeequity",                       # Or path to database file if using sqlite3.
        "USER": "lifeequity",                             # Not used with sqlite3.
        "PASSWORD": "lifeequity",                         # Not used with sqlite3.
        "HOST": "localhost",                             # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",                             # Set to empty string for default. Not used with sqlite3.
    },
    "life_equity": {
        #"ENGINE": "django.db.backends.postgresql_psycopg2", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".
        "ENGINE": "sql_server.pyodbc", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".       
        "NAME": "ITISERVER",                       # Or path to database file if using sqlite3.
        #"HOST": "10.0.0.4",                             # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "10.32.31.14",                             # Set to empty string for localhost. Not used with sqlite3.
        "USER": "rob.tucker",                             # Not used with sqlite3.
        "PASSWORD": "Password1",                         # Not used with sqlite3.
        "PORT": "1433",                             # Set to empty string for default. Not used with sqlite3.
        'OPTIONS' : {
            'driver': 'FreeTDS',
            'host_is_server': True,
            'extra_params': "TDS_VERSION=8.0"
        },
    }
}
DATABASE_ROUTERS = ['DatabaseRouter.LeapRouter', ]
TIME_ZONE = "US/Eastern"
LANGUAGE_CODE = "en-us"


SECRET_KEY = "secret key in here that you should never share"

# List of callables that know how to import templates from various sources.


ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_USE_OPENID = False
ACCOUNT_REQUIRED_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = False
ACCOUNT_EMAIL_AUTHENTICATION = False
ACCOUNT_UNIQUE_EMAIL = EMAIL_CONFIRMATION_UNIQUE_EMAIL = True
AGE_DISTRIBUTION_COLORS = [
        {'label': "Female - 74-Under", 'color': '#FFCCFF'},
        {'label': "Female - 75 to 79",'color':'#FF99CC'},
        {'label': "Female - 80 to 84",'color':'#FF0066'},
        {'label': "Female - 85+",'color':'#FF0000'},
        {'label': "Male - 74-Under", 'color': '#99CCFF'},
        {'label': "Male - 75 to 79",'color':'#6699FF'},
        {'label': "Male - 80 to 84",'color':'#0066FF'},
        {'label': "Male - 85+",'color':'#003399'},
        ]
GENERATED_SERIES_STYLES = [
        {'stroke':'#3300FF', 'fill': '#3300FF'},
        {'stroke':'#3388DD', 'fill': '#3388DD'},
        {'stroke':'#33DD88', 'fill': '#33DD88'},
        {'stroke':'#66FF88', 'fill': '#66FF88'},
        {'stroke':'#99CC55', 'fill': '#99CC55'},
        {'stroke':'#999922', 'fill': '#999922'},
        {'stroke':'#996611', 'fill': '#996611'},
        {'stroke':'#992200', 'fill': '#992200'},
        {'stroke':'#00CC22', 'fill': '#00CC22'},
        {'stroke':'#FF0000', 'fill': '#FF0000'},
        {'stroke':'#FF5500', 'fill': '#FF5500'},
        {'stroke':'#FF9900', 'fill': '#FF9900'},
        {'stroke':'#FFEE00', 'fill': '#FFEE00'},
        {'stroke':'#FFFF22', 'fill': '#FFFF22'},
        {'stroke':'#FF9933', 'fill': '#FF9933'},
        {'stroke':'#FF4455', 'fill': '#FF4455'},
        {'stroke':'#FF9966', 'fill': '#FF9966'},
        {'stroke':'#FFBB66', 'fill': '#FFBB66'},
        {'stroke':'#FF11BB', 'fill': '#FF11BB'},
        {'stroke':'#FF11DD', 'fill': '#FF11DD'},
        {'stroke':'#9911EE', 'fill': '#9911EE'},
        {'stroke':'#CC22EE', 'fill': '#CC22EE'},
        {'stroke':'#00FF99', 'fill': '#00FF99'},
        {'stroke':'#CC6677', 'fill': '#CC6677'},
        {'stroke':'#CCFF11', 'fill': '#CCFF11'},
        {'stroke':'#CCAA33', 'fill': '#CCAA33'},
        {'stroke':'#CC5511', 'fill': '#CC5511'},
        {'stroke':'#990000', 'fill': '#990000'},
        {'stroke':'#99CC00', 'fill': '#99CC00'},
        {'stroke':'#005566', 'fill': '#005566'},
        ]
"""
    Expiration settings. 
    Automatically expire sessions after 20 minutes
"""
#SESSION_COOKIE_AGE = 60 * 20
#SESSION_SAVE_EVERY_REQUEST = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}
CACHE_BACKEND = 'django.core.cache.backends.memcached.MemcachedCache://localhost:11211'
CACHE_COUNT_TIMEOUT = 3600
INSURED_DOCS_LINK = "https://leapdocs.lifeequity.com/AppNet/FolderPop/FolderPop.aspx?FT=217&KT259_0_0_0=%s&Username=%s&Password=%s"
POLICY_DOCS_LINK = "https://leapdocs.lifeequity.com/AppNet/FolderPop/FolderPop.aspx?FT=210&KT117_0_0_0=%s&Username=%s&Password=%s"
FUNDER_FILING_CABINET = "https://leapdocs.lifeequity.com/AppNet/FolderPop/FolderPop.aspx?ID=753452&username=%s&Password=%s"
FUNDER_DOCS_RECENT = "https://leapdocs.lifeequity.com/AppNet/docpop/docpop.aspx?clienttype=%s&cqid=107&Username=%s&Password=%s"
ONBASE_ALLOWED_USER = [
        'rtucker',
        'admin',
        'bpetrosky',
        ]
DEFAULT_ONBASE_USERNAME = 'chronos'
DEFAULT_ONBASE_PASSWORD = 'cases'
try:
   from local_settings import *
except Exception, e:
    pass
SESSION_COOKIE_AGE = 20 * 60
SESSION_SAVE_EVERY_REQUEST = True
