from django.conf import settings
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.views.decorators.cache import cache_page
from django.contrib import admin
admin.autodiscover()



#handler500 = "pinax.views.server_error"


urlpatterns = patterns("",
   # url(r"^$", cache_page(60)(direct_to_template), { "template": "homepage.html", }, name="home"),
    #url(r"^/]$", direct_to_template, { "template": "demo.html", }, name="demo"),
    #url(r"^$", direct_to_template, { "template": "demo.html", }, name="demo"),
    url(r"^$", 'leap.views.index', name="leap_index"),
    url(r"^login[/]$", 'login.views.login_view', name="login"),
    #url(r"^demo/charts[/]$", direct_to_template, { "template": "charts.html", }, name="charts"),
    url(r"^leap/denied[/]$", direct_to_template, { "template": "charts.html", }, name="denied"),
    #url(r"^demo/policy_detail/(?P<policy_id>.*)[/]$", direct_to_template, { "template": "policy_detail.html", }, name="policy_detail"),
    #url(r"demo[/]", direct_to_template, {
    #    "template": "demo.html",
    #}, name="demo"),
    #url(r"^admin/invite_user/$", "pinax.apps.signup_codes.views.admin_invite_user", name="admin_invite_user"),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^leap/", include("leap.urls")),
    #url(r"^account/", include("pinax.apps.account.urls")),
    #url(r"^profiles/", include("idios.urls")),
    #url(r"^notices/", include("notification.urls")),
    #url(r"^letter/", include("letter.urls")),
    #url(r"^announcements/", include("announcements.urls")),
)


if settings.SERVE_MEDIA:
    urlpatterns += patterns("",
        url(r"", include("staticfiles.urls")),
    )

