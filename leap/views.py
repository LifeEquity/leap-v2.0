# Create your views here.
from __future__ import division
from django.db.models import Sum, Count
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from local_app.models import ReadAlerts
from django.views.decorators.cache import cache_page
from operator import itemgetter
from django.contrib.humanize.templatetags.humanize import intcomma
import models
import json
#import forms
import datetime
import time
from django.contrib.humanize.templatetags.humanize import intcomma
from settings import AGE_DISTRIBUTION_COLORS, GENERATED_SERIES_STYLES
import re

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def index(request):
    report_table = []
    active_policy_sum = 0
    list = models.LEAP_Policy.objects.all()[:10]
    fund_list = get_fund_list(request), 
    policy_list = get_policy_list(request)
    active_policies = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Active - In-Force'))

    death_claims_collected_list = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').values('PolicyStatus').filter(SubFundID__in=policy_list).filter(PolicyStatus='Matured')
    death_claims_collected_list_sum = death_claims_collected_list.annotate(Sum('CurrentNDB'))
    death_claims_collected_list_count = death_claims_collected_list.annotate(Count('PolicyID'))

    death_claims_pending_list = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').values('PolicyStatus').filter(SubFundID__in=policy_list).filter(PolicyStatus__icontains='Death Claim')
    death_claims_pending_list_sum = death_claims_pending_list.annotate(Sum('CurrentNDB'))
    death_claims_pending_list_count = death_claims_pending_list.annotate(Count('PolicyID'))

    death_claims_collected_sum = 0
    death_claims_collected_count = 0
    death_claims_pending_sum = 0
    death_claims_pending_count = 0
    for dc in death_claims_collected_list_sum:
        death_claims_collected_sum += int(dc['CurrentNDB__sum'])
    for dc in death_claims_collected_list_count:
        death_claims_collected_count += int(dc['PolicyID__count'])
    for dc in death_claims_pending_list_sum:
        death_claims_pending_sum += int(dc['CurrentNDB__sum'])
    for dc in death_claims_pending_list_count:
        death_claims_pending_count += int(dc['PolicyID__count'])
    death_claims_pending = 0
    active_policy_count = active_policies.count()
    active_policy_sum_list   = active_policies.values('PolicyStatus').annotate(Sum('CurrentNDB'))
    gender_active_insured_list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
    active_policy_count_male = gender_active_insured_list.filter(Gender='Male').count()
    active_policy_count_female = gender_active_insured_list.filter(Gender='Female').count()
    #active_policy_sum_list   = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').values('PolicyStatus').annotate(Sum('CurrentNDB')).filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Active - In-Force'))
    for l in active_policy_sum_list:
        try:
            active_policy_sum += int(l['CurrentNDB__sum'])
        except:
            pass
    read_alerts = [a.AlertID for a in request.user.readalerts_set.all()]
    alerts = models.LEAP2_Alerts.objects.filter(SubFundID__in=policy_list).exclude(UniqueID__in=read_alerts).order_by('AlertDateTime')
    insurance_company_distribution_data_result = insurance_company_distribution_data(request, -1)
    report_table.append({'Total Policies':'<a href="%s">%s</a>' % (reverse('active_policy_list'), active_policy_count)})
    report_table.append({'Total NDB':'<a href="%s">%s</a>' % (reverse('active_policy_list'), currency(active_policy_sum))})
    report_table.append({'Male': '<a href="%s?gender=Male">%s</a>' % (reverse('active_insured_list'), active_policy_count_male)})
    report_table.append({'Female': '<a href="%s?gender=Female">%s</a>' % (reverse('active_insured_list'), active_policy_count_female)})
    report_table.append({'Death Claims Collected': '<a href="%s">%s / %s</a>' % (reverse('maturity_claims'), currency(death_claims_collected_sum), death_claims_collected_count)})
    report_table.append({'Death Claims Pending': '<a href="%s">%s / %s</a>' % (reverse('maturity_claims'), currency(death_claims_pending_sum), death_claims_pending_count)})
    report_table.append({'Largest Carrier Concentration': '<a href="%s">%s</a>' % (reverse('insurance_companies_list'), insurance_company_distribution_data_result[0]['label'])})
    report_table.append({'Funder Filing Cabinet': '<a target="_blank" href="%s">View</a>' % (reverse('document_viewer', kwargs={'id': '0', 'type':'funder_filing_cabinet'}))})
    report_table.append({'Recent Docs': '<a target="_blank" href="%s">View</a>' % (reverse('document_viewer', kwargs={'id': '0', 'type':'last_seven'}))})
    return render_to_response('leap/dashboard.html', {
        'fund_list': get_fund_list(request),
        'alerts': alerts,
        'alert_count': len(alerts),
        'list':list,
        'report_table': report_table,
        'page_title': 'LEAP - Dashboard',
        'premiums_chart_data': json.dumps(calculate_premiums_chart(request)),
        'report_by_age_sex_data': json.dumps(le_report_by_age_sex_data(request)),
        'generated_series_styles': json.dumps(generated_series_styles()),
        'active_page':'home_page',
        'show_multi_select': True,
        }, 
        RequestContext(request) )
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def charts(request):
    list = models.LEAP_Policy.objects.all()[:10]
    gender_abd = gender_age_breakdown_data(request)
    gender_adb_colors = []
    fund_list = get_fund_list(request), 
    policy_list = get_policy_list(request)
    insurance_company_distribution_data_result = insurance_company_distribution_data(request, 10)
    for g in gender_abd:
        gender_adb_colors.append({
                'stroke': g['color'],
                'fill': g['color'],
                "stroke-width": "1.5",
            })
    return render_to_response('leap/charts.html', {
        'fund_list': get_fund_list(request),
        'list':list,
        'page_title': 'LEAP - Dashboard',
        'remaining_le_distribution_chart_data': json.dumps(remaining_le_distribution_chart_data(request)),
        'generated_series_styles': json.dumps(generated_series_styles()),
        'age_distribution_colors': json.dumps(gender_adb_colors),
        'gender_age_breakdown_data_json': json.dumps(gender_abd),
        'gender_age_breakdown_data': gender_abd,
        'insurance_company_distribution_data_json': json.dumps(insurance_company_distribution_data_result),
        'insurance_company_distribution_data': insurance_company_distribution_data_result,
        'active_page':'charts',
        'show_multi_select': True,
        }, 
        RequestContext(request) )
def generated_series_styles():
    ret = []
    for g in GENERATED_SERIES_STYLES:
        ret.append({'fill':g['fill'], 'stroke':g['stroke'], 'stroke-width':'1.5'})
    return ret


@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
@csrf_exempt
def premium_pending(request):
    columns = ['Due Date', 'Policy', 'Premium Due', 'Loan Amount Due', 'Total Due', ]
    if request.GET.get('date_range'):
        try:
            date_list = request.GET.get('date_range').split('-')
            start_date = date_list[0].strip()
            end_date = date_list[1].strip()
        except:
            end_date = start_date
    elif request.GET.get('month'):
        start_date = datetime.datetime.strptime(request.GET.get('month'), '%m-%y')
        end_date = start_date
        start_date = datetime.datetime.strftime(start_date, "%m/%d/%Y" ).lstrip("0")
        end_date = datetime.datetime.strftime(end_date, "%m/%d/%Y" ).lstrip("0")

    else:
        end_date = datetime.datetime.today() + datetime.timedelta(90)
        start_date = datetime.datetime.strftime(datetime.datetime.today(), "%m/%d/%Y" ).lstrip("0")
        end_date = datetime.datetime.strftime(end_date, "%m/%d/%Y" ).lstrip("0")
    date_string = "%s-%s" % (start_date, end_date)
    
    try:
        page_title = 'Pending Premiums'
        fund_list = get_fund_list(request), 
        policy_list = get_policy_list(request)
        time_format = "%m/%d/%Y"
        start_date = datetime.datetime.fromtimestamp(time.mktime(time.strptime(start_date, time_format)))
        if start_date < datetime.datetime.today():
            start_date = datetime.datetime.today()
        end_date = datetime.datetime.fromtimestamp(time.mktime(time.strptime(end_date, time_format)))
        """
            Installing new model per revision 2 list
            retlist = models.LEAP2_PremiumInfo.objects.filter(SubFundID__in=policy_list).filter(Q(DueDate__gte=start_date) & Q(DueDate__lte=end_date)).order_by('DueDate')
        """
        retlist = models.LEAP2_PremiumInfoPending.objects.filter(SubFundID__in=policy_list).filter(Q(DueDate__gte=start_date) & Q(DueDate__lte=end_date) & Q(PolicyStatus__contains='Active')).order_by('DueDate')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                try:
                   due_date = l.DueDate.strftime("%m/%d/%Y")
                except:
                    due_date = ''

                tmp.append([
                    str(due_date),
                    str(l.FunderPolicyID),
                    str(l.PremiumDue),
                    str(l.LoanAmtDue),
                    str(l.TotalDue),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/pending_premium_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'premiums_chart_data': json.dumps(calculate_premiums_chart_by_date(request, start_date, end_date)),
                'columns':columns,
                'date_string': date_string,
                'show_multi_select':True,
                'page_title': 'Pending Premiums',
                'active_page': 'premium_pending',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
@csrf_exempt
def premium_grace(request):
    columns = ['Due Date', 'Policy', 'Amount Due', 'Loan Amount Due', 'Total Due', ]
    try:
        page_title = 'Grace Events'
        fund_list = get_fund_list(request), 
        policy_list = get_policy_list(request)
        list = models.LEAP_PremiumHistoryInfo.objects.filter(PremiumReceived=0).filter(SubFundID__in=policy_list).order_by('DueDate')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(export_DOD),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/pending_premium_list.html', {
                'fund_list': get_fund_list(request), 
                'list':list,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page': 'premium_grace',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@login_required
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
@csrf_exempt
def premium_history(request):
    columns = ['Due Date', 'Funder Policy ID', 'Amt Due', 'Prem Applied', 'Prem Applied Date', 'AV', 'CSV', 'Prem Conf Method']
    if request.GET.get('date_range'):
        try:
            date_list = request.GET.get('date_range').split('-')
            start_date = date_list[0].strip()
            end_date = date_list[1].strip()
        except:
            end_date = start_date
    else:
        end_date = datetime.datetime.strftime(datetime.datetime.today(), "%m/%d/%Y" )
        start_date = datetime.datetime.today() - datetime.timedelta(90)
        start_date = datetime.datetime.strftime(start_date, "%m/%d/%Y" )
    #tmp = start_date
    #start_date = end_date
    #end_date = tmp
    date_string = "%s-%s" % (start_date, end_date)

    try:
        page_title = 'Premium History'
        fund_list = get_fund_list(request), 
        policy_list = get_policy_list(request)
        time_format = "%m/%d/%Y"
        start_date = datetime.datetime.fromtimestamp(time.mktime(time.strptime(start_date, time_format)))
        end_date = datetime.datetime.fromtimestamp(time.mktime(time.strptime(end_date, time_format)))
        retlist = models.LEAP2_PremiumInfo.objects.filter(SubFundID__in=policy_list).filter(Q(DueDate__gte=start_date) & Q(DueDate__lte=end_date)).order_by('DueDate')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                """export_DOB = ""
                export_DOD = ""
                if l.SSN:
                    l.SSN = '***-**-%s' % l.SSN[7:12]
                else:
                    l.SSN = '***-**-***'
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(export_DOD),
                    str(l.SSN if l.SSN else ''),
                    str(l.Gender if l.Gender else ''),
                    str(l.MaritalStatus if l.MaritalStatus else ''),
                    str(l.FunderInsuredID if l.FunderInsuredID else ''),
                    ])"""
                tmp.append([
                    str(l.DueDate.strftime("%m/%d/%Y")) if l.DueDate else '',
                    str(l.FunderPolicyID) if l.FunderPolicyID else '',
                    str(l.PremiumDue) if l.PremiumDue else '',
                    str(l.PremiumReceived) if l.PremiumReceived else '',
                    str(l.PremiumReceivedDate.strftime("%m/%d/%Y")) if l.PremiumReceivedDate else '',
                    str(l.AV) if l.AV else '',
                    str(l.CSV) if l.CSV else '',
                    str(l.PremConfMethod) if l.PremConfMethod else '',
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/history_premium_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'premiums_chart_data': json.dumps(calculate_premiums_chart_by_date(request, start_date, end_date)),
                'columns':columns,
                'date_string': date_string,
                'show_multi_select':True,
                'page_title': 'Premium History',
                'active_page': 'premium_history',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
@csrf_exempt
def insured_list(request):
    columns = ['Name', 'DOB', 'DOD', 'SSN', 'Gender', 'Marital Status', 'Funder Insured ID']
    try:
        policy_age = request.GET['policy_age']
        policy_gender = request.GET['policy_gender']
        page_title = 'Insured List %s - %s' % (policy_gender, policy_age)
        policy_list = get_policy_list(request)
        #list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(LatestLEAgeinMos=policy_age).filter(Gender=policy_gender).filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
        list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(LatestLEAgeinMos=policy_age).filter(Gender=policy_gender).filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
        #.defer('PolicyID').defer('FundID').defer('SubFundID').filter(LatestLEAgeinMos=policy_age).filter(Gender=policy_gender).filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
        for l in list:
            l.SSN = '***-**-%s' % l.SSN[7:12]
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(export_DOD),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/insured_list.html', list, page_title, columns, 'active_insured_list', True)
    except Exception, e:
        return HttpResponseForbidden('You have supplied insufficient parameters')

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def active_insured_list(request):
    columns = ['Name', 'DOB', 'DOD', 'SSN', 'Gender', 'Marital Status', 'Funder Insured ID']
    try:
        page_title = 'Active Insured List'
        policy_list = get_policy_list(request)
        #list = models.LEAP2_Insured_from_PC.objects.active_in_policy_list(policy_list)
        list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
        gender = request.GET.get('gender')
        if gender:
            list = list.filter(Gender=gender)

        for l in list:
            l.SSN = '***-**-%s' % l.SSN[7:12]
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(export_DOD),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/insured_list.html', list, page_title, columns,'active_insured_list',  True)
    except Exception, e:
        return HttpResponseForbidden(e)

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def outdated_insured_list(request):
    columns = ['Name', 'DOB', 'SSN', 'Gender', 'Marital Status', 'Funder Insured ID', 'Report Age']
    try:
        page_title = 'Outdated Insured List'
        table_title = 'Outdated Life Expectancies - no LE on file or most recent LE more than 12 months old.'
        policy_list = get_policy_list(request)
        #list = models.LEAP2_Insured_from_PC.objects.outdated_in_policy_list(policy_list)
        list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active').filter(Q(AgeOfReport__gt=13) | Q(AgeOfReport=None)).order_by('-AgeOfReport')
        for l in list:
            l.SSN = '***-**-%s' % l.SSN[7:12]
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    str(l.LatestLEAgeinMos),
                    #str(l.AgeOfReport),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/insured_list.html', list, page_title, columns, 'outdated_insured_list', True, table_title)
    except Exception, e:
        return HttpResponseForbidden(e)

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def jobs_detail(request, id):
    columns = ['Job Number', 'Status', 'Description', 'Request Recd Date', 'Start Date', 'Target Completion Date', 'Complete Date']
    try:
        funds = []
        policy_list = get_policy_list(request)
        for s in policy_list:
            fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
            if fund_id not in funds:
                funds.append(fund_id)
        job = models.LEAP2_Jobs.objects.filter(FunderID__in=funds).get(JobNumber=id)
        insured_events = models.LEAP2_InsuredEvents.objects.filter(JobID=job.JobID).order_by('-RequestDate')
        policy_events = models.LEAP2_PolicyEvents.objects.filter(JobID=job.JobID)
        graph_data = []
        graph_data.append({'label' : 'Completed', 'data': job.CompletedEvents, 'color': '#FFD700'})
        graph_data.append({'label' : 'Incomplete', 'data': job.PendingEvents, 'color': '#CC3300'})
        #policy_events = models.LEAP2_PolicyEvents.objects.filter(JobID=job.JobID)
        ## This is just placeholder until I get the LEAP2_PolicyEvents view from scott
        page_title = 'Job Detail'
        return render_to_response('leap/job_detail.html', {
            'fund_list': get_fund_list(request),
            'job': job,
            'columns':columns,
            'graph_data': json.dumps(graph_data),
            'policy_events': policy_events,
            'insured_events': insured_events,
            'show_multi_select':False,
            'page_title': page_title,
            'active_page':'jobs_pending',
            }, 
            RequestContext(request) )
    except models.LEAP2_Jobs.DoesNotExist:
        return HttpResponse('You do not have access to this Job')
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def jobs_pending(request):
    columns = ['Job Number', 'Status', 'Description', 'Request Recd Date', 'Start Date', 'Target Completion Date', 'Complete Date']
    try:
        funds = []
        page_title = 'In Progress Jobs'
        policy_list = get_policy_list(request)
        for s in policy_list:
            fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
            if fund_id not in funds:
                funds.append(fund_id)
        retlist = models.LEAP2_Jobs.objects.filter(FunderID__in=funds).filter(JobStatus='Pending').order_by('JobID')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                tmp.append([
                    str(l.JobNumber.strip()) if l.JobNumber else '',
                    str(l.JobStatus.strip()) if l.JobStatus else '',
                    str(l.memo.strip()) if l.memo else '',
                    str(l.JobRequestReceived.strftime("%m/%d/%Y")) if l.JobRequestReceived else '',
                    str(l.JobStartDate.strftime("%m/%d/%Y")) if l.JobStartDate else '',
                    str(l.TargetCompleteDate.strftime("%m/%d/%Y")) if l.TargetCompleteDate else '',
                    str(l.JobComplete.strftime("%m/%d/%Y")) if l.JobComplete else '',
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/jobs_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page':'jobs_pending',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def jobs_completed(request):
    columns = ['Job Number', 'Status', 'Description', 'Request Recd Date', 'Start Date', 'Target Completion Date', 'Complete Date']
    try:
        funds = []
        page_title = 'Completed Jobs'
        policy_list = get_policy_list(request)
        for s in policy_list:
            fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
            if fund_id not in funds:
                funds.append(fund_id)
        retlist = models.LEAP2_Jobs.objects.filter(FunderID__in=funds).filter(JobStatus='Complete').order_by('JobID')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    str(l.LatestLEAgeinMos),
                    #str(l.AgeOfReport),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/jobs_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page':'jobs_completed',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def insurance_companies_list(request):
    columns = ['Insurance Company Name', 'Number of Policies', '% NDB',]
    try:
        funds = []
        page_title = 'Insurance Companies'
        policy_list = get_policy_list(request)
        insurance_company_distribution_data_result = insurance_company_distribution_data(request, 30)
        for s in policy_list:
            fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
            if fund_id not in funds:
                funds.append(fund_id)
        retlist = insurance_company_distribution_data_result
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    str(l.LatestLEAgeinMos),
                    #str(l.AgeOfReport),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/insurance_companies_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'insurance_company_distribution_data_json': json.dumps(insurance_company_distribution_data_result),
                'insurance_company_distribution_data': insurance_company_distribution_data_result,
                'show_multi_select':True,
                'generated_series_styles': json.dumps(generated_series_styles()),
                'page_title': page_title,
                'active_page':'insurance_companies',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def life_expectancy_list(request):
    columns = ['Name', 'DOB', 'SSN', 'Gender', 'Marital Status', 'Funder Insured ID', 'Report Age']
    try:
        funds = []
        page_title = 'Life Expectancy List'
        policy_list = get_policy_list(request)
        for s in policy_list:
            fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
            if fund_id not in funds:
                funds.append(fund_id)
        retlist = models.LEAP2_LifeExpectanciesWithInsured.objects.filter(FunderID__in=funds).order_by('-ReportDate')[:500]
        if request.method == "POST":
            tmp = []
            tmp.append(['Report Date',
                'Provider',
                '50% Months',
                '85% Months',
                'LE Mortality',
                'Primary Diagnosis',
                'Insured Name'])
            for l in retlist:
                tmp.append([
                    str(l.ReportDate.strftime("%m/%d/%Y")) if l.ReportDate else '',
                    str(l.Provider.strip()) if l.Provider else '',
                    str(l.LEMonths50) if l.LEMonths50 else '',
                    str(l.LEMonths85) if l.LEMonths85 else '',
                    str(l.LEMortality) if l.LEMortality else '',
                    str(l.PrimaryDiagnosis.strip()) if l.PrimaryDiagnosis else '',
                    str(l.FullNameLastFirst.strip()) if l.FullNameLastFirst else '',
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/life_expectancy_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page':'life_expectancy_list',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def read_alert(request, id):
    user_mark_alert_read(request.user, id) 
    return HttpResponseRedirect(reverse('alerts'))

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def document_viewer(request, type, id):
    from settings import STATIC_URL, ONBASE_ALLOWED_USER, DEFAULT_ONBASE_USERNAME, DEFAULT_ONBASE_PASSWORD, POLICY_DOCS_LINK, INSURED_DOCS_LINK, FUNDER_FILING_CABINET, FUNDER_DOCS_RECENT
    username = request.user.username
    if request.user.username not in ONBASE_ALLOWED_USER:
        try:
            password = models.LEAP2_Credentials.objects.filter(Username=username)[0].Password
        except Exception, e:
            username = DEFAULT_ONBASE_USERNAME
            password = DEFAULT_ONBASE_PASSWORD
            pass
    else:
        username = DEFAULT_ONBASE_USERNAME
        password = DEFAULT_ONBASE_PASSWORD

    if type == 'policy':
        iframe_src = POLICY_DOCS_LINK % (id, username, password)

    elif type == 'insured':
        i = models.LEAP2_Insured_from_PC.objects.filter(InsuredID=id)[0].clear_ssn
        iframe_src = INSURED_DOCS_LINK % (i, username, password)

    elif type == 'funder_filing_cabinet':
        iframe_src = FUNDER_FILING_CABINET % (username, password)
    elif type == 'last_seven':
        if 'MSIE' in request.META['HTTP_USER_AGENT']:
            ua_content_type = 'activex'
        else:
            ua_content_type = 'html'

        iframe_src = FUNDER_DOCS_RECENT % (ua_content_type, username, password)
    output = '''<html><head></head>
            <body style="margin: 0px; overflow: hidden; border: none;">
            <div style="float: left; border: none; padding: 0px; margin: 0px; margin-top: 4px; margin-bottom: -12px; overflow: hidden; width: 100%%; height: 40px;">
            <a style="color: rgb(50, 159, 204);" href="%sswf/HylandWebActiveXControls.msi">Download ActiveX Controls</a>
            </div>
            <iframe scrolling="no" frameBorder="none" border="none" style="float: left; border: none; padding: 0px; margin: 0px; overflow: hidden; width: 100%%; height: 100%%;" src="%s" />
            </body></html>''' % (STATIC_URL, iframe_src)
    return HttpResponse(output)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def ack_alert_by_type(request, type, alert_id, id):
    a = ReadAlerts(user=request.user, AlertID=alert_id).save()
    if type == 'policy':
        return HttpResponseRedirect(reverse('policy_detail', kwargs={'policy_number': id}))

    if type == 'insured':
        return HttpResponseRedirect(reverse('insured_detail', kwargs={'insured_id': id}))

def user_mark_alert_read(user, id):
    try:
        a = ReadAlerts(user=user, AlertID=id).save()
        return True
    except Exception, e:
        print e
        return False

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def alerts(request):
    columns = ['Type', 'Date', 'Note', 'Link', 'Action']
    try:
        page_title = 'Alerts'
        policy_list = get_policy_list(request)
        read = []
        unread = []
        retlist = models.LEAP2_Alerts.objects.filter(SubFundID__in=policy_list).order_by('-AlertDateTime')
        if request.method == "POST":
            if "mark_read" in request.POST:
                for a in request.POST.getlist("mark_read_list[]"):
                    user_mark_alert_read(request.user, int(a))
                return HttpResponseRedirect(reverse('alerts'))
            else:
                tmp = []
                tmp.append(['Type', 'Date', 'Note'])
                for l in retlist:
                    alert_date = l.AlertDateTime.strftime("%m/%d/%Y") if l.AlertDateTime else ''
                    tmp.append([
                            str(l.AlertType.strip()),
                            str(alert_date),
                            str(l.AlertString.strip()),
                        ])
                return xls_simple(request, tmp, page_title)
        else:
            for r in retlist:
                if r.is_read:
                    read.append(r)
                else:
                    unread.append(r)
            retlist = []
            retlist = unread + read
            return render_to_response('leap/alerts_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page':'alerts',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def policy_list(request):
    columns = ['Sub Fund', 'Policy Number', 'Funder Policy ID', 'CurrentNDB', 'Insurance CO', 'Insured Name', 'Policy Status']
    try:
        age = request.GET['policy_age']
        if age == '0-3':
            age = '0-3 years'
        if age == '4-7':
            age = '4-7 years'
        if age == '8-11':
            age = '8-11 years'
        if age == '12-15':
            age = '12-15 years'
        gender = request.GET['policy_gender']
        policy_list = get_policy_list(request)
        #list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(LatestLEAgeinMos=policy_age).filter(Gender=policy_gender).filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
        #retlist = models.LEAP2_PolicyDetail.objects.active_in_policy_list_by_le_age_and_gender(policy_list, age, gender)
        retlist = models.LEAP2_PolicyDetail.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active - In-Force')| Q(PolicyStatus='Active')).filter(LERemainingGroup=age)#.filter(Gender=gender)
        page_title = 'Active Policy List'
        policy_list = get_policy_list(request)
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                tmp.append([
                        str(l.SubFundName.strip()),
                        str(l.PolicyNumber.strip()),
                        str(l.FunderPolicyID.strip()),
                        str(l.CurrentNDB),
                        str(l.InsuranceCompanyName.strip()),
                        str(l.FullNameLastFirst.strip()),
                        str(l.PolicyStatus.strip()),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/policy_list.html', retlist, page_title, columns, 'active_policy_list', True)
    except Exception, e:
        return HttpResponseForbidden(e)

@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def active_policy_list(request):
    columns = ['Sub Fund', 'Policy Number', 'Funder Policy ID', 'Current NDB', 'Insurance CO', 'Insured Name', 'Policy Status']
    try:
        page_title = 'Active Policy List'
        policy_list = get_policy_list(request)
        gender = request.GET.get('gender', None)
        retlist = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Active - In-Force'))

        ## Somewhat hacky workaround for mssql flushing it's query cache and not returning a correct recordset
        if retlist is False:
            retlist = models.LEAP2_PolicyDetail.objects.defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Active - In-Force'))

        if gender:
            retlist = retlist.filter(Gender=gender)
        #retlist = []
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                tmp.append([
                        str(l.SubFundName.strip() if l.SubFundName else ''),
                        str(l.PolicyNumber.strip() if l.PolicyNumber else ''),
                        str(l.FunderPolicyID.strip() if l.FunderPolicyID else ''),
                        str(l.CurrentNDB if l.CurrentNDB else ''),
                        str(l.InsuranceCompanyName.strip() if l.InsuranceCompanyName else ''),
                        str(l.FullNameLastFirst.strip() if l.FullNameLastFirst else ''),
                        str(l.PolicyStatus.strip() if l.PolicyStatus else ''),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/policy_list.html', retlist, page_title, columns, 'active_policy_list', True)
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def grace_events_list(request):
    columns = ['Notice Date', 'Funder Policy ID', 'Lapse Date', 'Grace Status', 'Date Entered Grace', 'Total Amount Due', 'Paid Date', 'Paid Amount']
    try:
        page_title = 'Grace Event List'
        policy_list = get_policy_list(request)
        retlist = models.LEAP2_GraceEvents.objects.filter(SubFundID__in=policy_list).filter(GraceStatus='Pending').order_by('-LapseDate')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:

                if l.LapseDate:
                    lapse_date = l.LapseDate.strftime("%m/%d/%Y")
                else:
                    lapse_date = ''

                if l.NoticeDate:
                    notice_date = l.NoticeDate.strftime("%m/%d/%Y")
                else:
                    notice_date = ''

                if l.DateEnteredGrace:
                    date_entered_grace = l.DateEnteredGrace.strftime("%m/%d/%Y")
                else:
                    date_entered_grace = ''

                if l.PaidDate:
                    paid_date = l.PaidDate.strftime("%m/%d/%Y")
                else:
                    paid_date = ''

                tmp.append([
                        str(notice_date),
                        str(l.PolicyNumber),
                        str(lapse_date),
                        str(l.GraceStatus.strip()),
                        str(date_entered_grace),
                        str(l.TotalAmountDue),
                        str(paid_date),
                        str(l.PaidAmount),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/grace_list.html', {
                'fund_list': get_fund_list(request), 
                'list':retlist,
                'columns':columns,
                'show_multi_select':True,
                'page_title': page_title,
                'active_page':'grace_events_list',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def inactive_policy_list(request):
    columns = ['Sub Fund', 'Policy Number', 'Funder Policy ID', 'Current NDB', 'Insurance CO', 'Insured Name', 'Policy Status']
    try:
        page_title = 'Inactive Policy List'
        policy_list = get_policy_list(request)
        retlist = models.LEAP2_Policy_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter( Q(PolicyStatus='Inactive') | Q(PolicyStatus='Lapsed - Per Funder') )
        #retlist = models.LEAP2_Policy_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(Q(InsuredStatus='Inactive') | Q(InsuredStatus='Deceased'))
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in retlist:
                tmp.append([
                        str(l.SubFundName.strip()),
                        str(l.PolicyNumber.strip()),
                        str(l.FunderPolicyID.strip()),
                        str(l.CurrentNDB),
                        str(l.InsuranceCompanyName.strip()),
                        str(l.FullNameLastFirst.strip()),
                        str(l.PolicyStatus.strip()),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/policy_list.html', retlist, page_title, columns, 'inactive_policy_list', True)
    except Exception, e:
        return HttpResponseForbidden(e)
@csrf_exempt
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def inactive_insured_list(request):
    columns = ['Name', 'DOB', 'DOD', 'SSN', 'Gender', 'Marital Status', 'Funder Insured ID']
    try:
        page_title = 'Inactive Insured List'
        policy_list = get_policy_list(request)
        #list = models.LEAP2_Insured_from_PC.objects.inactive_in_policy_list(policy_list)
        list = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(Q(InsuredStatus='Inactive') | Q(InsuredStatus='Deceased'))
        for l in list:
            l.SSN = '***-**-%s' % l.SSN[7:12]
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                export_DOB = ""
                export_DOD = ""
                l.SSN = '***-**-%s' % l.SSN[7:12]
                if l.DOB is not None and l.DOB != '':
                    export_DOB = l.DOB.strftime("%m/%d/%Y")
                if l.DOD is not None and l.DOD != '':
                    export_DOD = l.DOD.strftime("%m/%d/%Y")
                tmp.append([
                    str(l.FullNameLastFirst),
                    str(export_DOB),
                    str(export_DOD),
                    str(l.SSN),
                    str(l.Gender),
                    str(l.MaritalStatus),
                    str(l.FunderInsuredID),
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return insured_generic(request, 'leap/insured_list.html', list, page_title, columns, 'inactive_insured_list', True)
    except Exception, e:
        return HttpResponseForbidden(e)

def premium_generic(request, template, list, page_title, columns, active_page, show_multi_select):
    return render_to_response(template, {
        'fund_list': get_fund_list(request), 
        'list':list,
        'columns':columns,
        'show_multi_select':show_multi_select,
        'page_title': page_title,
        'active_page':active_page,
        }, 
        RequestContext(request) )

def insured_generic(request, template, list, page_title, columns, active_page, show_multi_select, table_title=None):
    return render_to_response(template, {
        'fund_list': get_fund_list(request), 
        'list':list,
        'columns':columns,
        'show_multi_select':show_multi_select,
        'page_title': page_title,
        'table_title': table_title,
        'active_page':active_page,
        }, 
        RequestContext(request) )


@login_required
@csrf_exempt
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def maturity_claims(request):
    columns = ['Status', 'Policy', 'NDB', 'Ins Co', 'DOD', "Date Notified", "Death Cert Rec'd", "Claim Filed", "Payment Rec'd"]
    try:
        page_title = 'Maturities'
        policy_list = get_policy_list(request)
        list = models.LEAP2_DeathClaim.objects.filter(SubFundID__in=policy_list).order_by('PolicyStatus').order_by('PolicyStatus')
        if request.method == "POST":
            tmp = []
            tmp.append(columns)
            for l in list:
                tmp.append([
                        str(l.DCStatus.strip()) if l.DCStatus else '',
                        str(l.FunderPolicyID.strip()) if l.FunderPolicyID else '',
                        str(l.CurrentNDB) if l.CurrentNDB else '',
                        str(l.InsuranceCompanyName.strip()) if l.InsuranceCompanyName else '',
                        str(l.DOD.strftime("%m/%d/%Y")) if l.DOD else '',
                        str(l.DateNotifiedDeath.strftime("%m/%d/%Y")) if l.DOD else '',
                        str(l.DeathCertCompleteDate.strftime("%m/%d/%Y")) if l.DeathCertCompleteDate else '',
                        str(l.DateDBClaimFiled.strftime("%m/%d/%Y")) if l.DateDBClaimFiled else '',
                        str(l.PaymentRecd.strftime("%m/%d/%Y")) if l.PaymentRecd else '',
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/maturity_claims_list.html', {
                'fund_list': get_fund_list(request), 
                'list':list,
                'columns':columns,
                'show_multi_select': True,
                'active_page':'maturity_claims',
                'page_title': 'Maturities',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)

@login_required
@csrf_exempt
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def monthly_valuations(request):
    columns = [
        'Policy Number',
        'Funder Policy ID',
        'Policy Status',
        'Current NDB',
        'Account Value',
        'Cash Surrender Value',
        'Date of AV/CSV',
        'Last COI Deduction',
        'Loan Amount',
        'Loan Amount Due',

]
    try:
        page_title = 'Monthly Valuations'
        policy_list = get_policy_list(request)
        list = models.LEAP2_MonthlyValuations.objects.filter(SubFundID__in=policy_list).order_by('PolicyStatus').order_by('PolicyStatus')
        if request.method == "POST":
            tmp = []
            columns.append("PrimAVS_LE")
            columns.append("PrimAVS_Date")
            columns.append("PrimAVS_Rating")
            columns.append("Prim21st_LE")
            columns.append("Prim21st_LEMean")
            columns.append("Prim21st_Date")
            columns.append("Prim21st_Rating")
            columns.append("SecAVS_LE")
            columns.append("SecAVS_Date")
            columns.append("SecAVS_Rating")
            columns.append("Sec21st_LE")
            columns.append("Sec21st_LEMean")
            columns.append("Sec21st_Date")
            columns.append("Sec21st_Rating")
            tmp.append(columns)
            for l in list:
                tmp.append([
                        str(l.PolicyNumber.strip()) if l.PolicyNumber else '',
                        str(l.FunderPolicyID.strip()) if l.FunderPolicyID else '',
                        str(l.PolicyStatus.strip()) if l.PolicyStatus else '',
                        str(l.CurrentNDB) if l.CurrentNDB else '',
                        str(l.AV) if l.AV else '',
                        str(l.CSV) if l.CSV else '',
                        str(l.DateOfAVCSV.strftime("%m/%d/%Y")) if l.DateOfAVCSV else '',
                        str(l.LastCOIDeduction) if l.LastCOIDeduction else '',
                        str(l.LoanAmount) if l.LoanAmount else '',
                        str(l.LoanAmountDate.strftime("%m/%d/%Y")) if l.LoanAmountDate else '',
                        str(l.PrimaryAVS_LE) if l.PrimaryAVS_LE else '',
                        str(l.PrimaryAVS_Date.strftime("%m/%d/%Y")) if l.PrimaryAVS_Date else '',
                        str(l.PrimaryAVS_Rating) if l.PrimaryAVS_Rating else '',
                        str(l.Primary21st_LE) if l.Primary21st_LE else '',
                        str(l.Primary21st_LEMean) if l.Primary21st_LEMean else '',
                        str(l.Primary21st_Date.strftime("%m/%d/%Y")) if l.Primary21st_Date else '',
                        str(l.Primary21st_Rating) if l.Primary21st_Rating else '',
                        str(l.SecondaryAVS_LE) if l.SecondaryAVS_LE else '',
                        str(l.SecondaryAVS_Date.strftime("%m/%d/%Y")) if l.SecondaryAVS_Date else '',
                        str(l.SecondaryAVS_Rating) if l.SecondaryAVS_Rating else '',
                        str(l.Secondary21st_LE) if l.Secondary21st_LE else '',
                        str(l.Secondary21st_LEMean) if l.Secondary21st_LEMean else '',
                        str(l.Secondary21st_Date.strftime("%m/%d/%Y")) if l.Secondary21st_Date else '',
                        str(l.Secondary21st_Rating) if l.Secondary21st_Rating else '',
                    ])
            return xls_simple(request, tmp, page_title)
        else:
            return render_to_response('leap/monthly_valuations.html', {
                'fund_list': get_fund_list(request), 
                'list':list,
                'columns':columns,
                'show_multi_select': True,
                'active_page':'monthly_valuations',
                'page_title': 'Monthly Valuations',
                }, 
                RequestContext(request) )
    except Exception, e:
        return HttpResponseForbidden(e)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def insured_detail(request, insured_id):
    #insured = get_object_or_404(models.LEAP2_Insured_from_PC, InsuredID=insured_id)
    insured_subfunds = []
    try:
        insureds = models.LEAP2_Insured_from_PC.objects.filter(InsuredID=insured_id)
        for sf in insureds:
            insured_subfunds.append(sf.SubFundID)
        insured = insureds[0]
    except:
        return HttpResponseForbidden('No Result Found')
    subfunds = [r.SubFundID for r in request.user.usersubfunds_set.all()]
    funds = []
    #if insured.SubFundID not in subfunds and not request.user.is_staff:
    # Set a variable to check for existance of funds associated with insured
    # vs what trhe user has access to
    found = False
    for sf in insured_subfunds:
        if sf in [u.SubFundID for u in request.user.usersubfunds_set.all()]:
            found = True

    if not found:
        return HttpResponseForbidden('You do not have access to this record')
    for s in subfunds:
        fund_id = models.LEAP2_SubFund_1180.objects.get(SubFundID=s).FundID
        if fund_id not in funds:
            funds.append(fund_id)

    life_expectancies = models.LEAP2_LifeExpectancy.objects.filter(InsuredID=insured_id).filter(FunderID__in=funds).order_by('-ReportDate')
    insured_events = models.LEAP2_InsuredEvents.objects.filter(InsuredID=insured_id).filter(FunderID__in=funds).order_by('-RequestDate')
    policies = models.LEAP2_PolicyDetail.objects.filter(Q(InsuredID=insured_id) | Q(InsuredID_Secondary=insured_id)).filter(SubFundID__in=subfunds)
    insured.SSN = '***-**-%s' % insured.SSN[7:12]
    return render_to_response('leap/insured_detail.html', {
        'funds': get_fund_list(request),
        'insured':insured,
        'insured_events':insured_events,
        'policies':policies,
        'life_expectancies':life_expectancies,
        'page_title': 'Insured Details - %s' % (insured.FunderInsuredID),
        }, RequestContext(request) )

def output_html_table(t_title, t_input):
    table = "<div id='sample-table-sortable' class='panel'>"
    table = "<table>"
    table += "<thead>"
    table += "<tr>"
    table += "<th colspan='2'>%s</th>" % (t_title)
    table += "</tr>"
    table += "</thead>"
    table += "<tbody>"
    for t in t_input:
        if not t['value']:
            t['value'] = ''
        table += "<tr>"
        table += "<td>%s:</td><td>%s</td>" % (t['label'], t['value'])
        table += "</tr>"
    table += "</tbody>"
    table += "</table>"
    table += "</div>"
    return HttpResponse(table)

    
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def insurance_company_table(request, id):
    data = []
    ins_co = get_object_or_404(models.LEAP2_InsCos, InsCoID=id)
    data.append({'label':'Insurance Company Name', 'value':ins_co.InsuranceCompanyName})
    data.append({'label':'Address 1', 'value':ins_co.Address1 })
    data.append({'label':'Address 2', 'value':ins_co.Address2 })
    data.append({'label':'City', 'value':ins_co.City })
    data.append({'label':'State', 'value':ins_co.State })
    data.append({'label':'Zip Code', 'value':ins_co.ZipCode })
    data.append({'label':'Phone Number', 'value':ins_co.PhoneNumber })
    if ins_co.Website:
        data.append({'label':'Website', 'value':'<a href="http://%s" target="_blank">View Website</a>' % ins_co.Website })
    data.append({'label':'Overnight Address 1', 'value':ins_co.OvernightAddress1 })
    data.append({'label':'Overnight Address 2', 'value':ins_co.OvernightAddress2 })
    data.append({'label':'Overnight City', 'value':ins_co.OvernightCity })
    data.append({'label':'Overnight State', 'value':ins_co.OvernightState })
    data.append({'label':'Overnight Zip Code', 'value':ins_co.OvernightZipCode })
    return output_html_table('Insurance Company Details', data)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def maturity_claim_table(request, id):
    l = models.LEAP2_DeathClaim.objects.get(PolicyID=id)
    data = []
    data.append({'label':'Status', 'value':l.DCStatus})
    data.append({'label':'Policy Number', 'value':l.PolicyNumber})
    data.append({'label':'Funder Policy ID', 'value':l.FunderPolicyID})

    if l.CurrentNDB:
        data.append({'label':'Death Benefit', 'value':'$%s' % l.CurrentNDB})
    else:
        data.append({'label':'Death Benefit', 'value':''})
    data.append({'label':'Insurance Company', 'value':'%s' % l.InsuranceCompanyName})
    
    try:
        data.append({'label':'DOD', 'value':l.DOD.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'DOD', 'value':''})

    try:
        data.append({'label':'Date Notified', 'value':l.DateNotifiedDeath.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Date Notified', 'value':''})

    data.append({'label':'Notification Method', 'value':l.DeathNotificationMethod})

    try:
        data.append({'label':'Death Certificate Start Date', 'value':l.DeathCertStartDate.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Death Certificate Start Date', 'value':''})

    try:
        data.append({'label':'Death Certificate Complete Date', 'value':l.DeathCertCompleteDate.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Death Certificate Complete Date', 'value':''})

    data.append({'label':'Death Certificate Comments', 'value':l.DeathCertMemo})
    
    try:
        data.append({'label':'Death Claim Start Date', 'value':l.DeathClaimStartDate.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Death Claim Start Date', 'value':''})
        
    try:
        data.append({'label':'Death Claim Complete Date', 'value':l.DeathClaimCompleteDate.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Death Claim Complete Date', 'value':''})
        
    data.append({'label':'Death Claim Notes', 'value':l.DeathClaimMemo})

    try:
        data.append({'label':'Payment Received', 'value':l.PaymentRecd.strftime("%m/%d/%Y")})
    except:
        data.append({'label':'Payment Received', 'value':''})

    return output_html_table('Maturity Claim Details', data)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def grace_event_table(request, id):
    l = models.LEAP2_GraceEvents.objects.get(GraceEventID=id)
    if not l.PaidAmount:
        l.PaidAmount = "0.00"
    if not l.Comments:
        l.Comments = ""
    try:
        conf_date = l.ConfirmedDate.strftime("%m/%d/%Y")
    except:
        conf_date = ''
    data = []
    data.append({'label':'Notice Date', 'value':l.NoticeDate.strftime("%m/%d/%Y")})
    data.append({'label':'Lapse Date', 'value':l.LapseDate.strftime("%m/%d/%Y")})
    data.append({'label':'Grace Status', 'value':l.GraceStatus})
    data.append({'label':'Date Entered Grace', 'value':l.DateEnteredGrace.strftime("%m/%d/%Y")})
    data.append({'label':'Total Amount Due', 'value':'$%s' % l.TotalAmountDue})
    #data.append({'label':'Assigned To', 'value':l.AssignedTo})
    data.append({'label':'Confirmed Date', 'value': conf_date})
    data.append({'label':'Paid Amount', 'value':'$%s' % l.PaidAmount})
    data.append({'label':'Comments', 'value':l.Comments})
    return output_html_table('Grace Event Details', data)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def premium_table(request, id):
    #l = models.LEAP_PremiumHistoryInfo.objects.get(PremiumID=id)
    l = models.LEAP2_PremiumInfo.objects.get(PremiumID=id)
    data = []

    data.append({'label':'Due Date', 'value':l.DueDate.strftime("%m/%d/%Y") if l.DueDate else ''})
    data.append({'label':'Amount Due', 'value':'$%s' % intcomma(l.PremiumDue) if l.PremiumDue else ''})
    data.append({'label':'Loan Payment Amount', 'value':"$%s" % l.LoanPaymentAmount.intcomma() if l.LoanPaymentAmount else ''})
    data.append({'label':'Total Due', 'value':"$%s" % intcomma(l.TotalDue) if l.TotalDue else ''})
    data.append({'label':'Payee name', 'value': l.Payee if l.Payee else ''})
    data.append({'label':'Loan Amount', 'value':'$%s' % l.LoanAmount if l.LoanAmount else ''})
    data.append({'label':'Loan Amount Date', 'value':l.LoanAmountDate.strftime("%m/%d/%Y") if l.LoanAmountDate else ''})
    data.append({'label':'Prem Applied Date', 'value':l.PremiumReceivedDate.strftime("%m/%d/%Y") if l.PremiumReceivedDate else ''})
    data.append({'label':'Prem Conf Method', 'value':l.PremConfMethod if l.PremConfMethod else ''})
    #data.append({'label':'Prem Conf Method', 'value':l.Comments})
    data.append({'label':'CSV', 'value':"$%s" % intcomma(l.CSV) if l.CSV else ''})
    data.append({'label':'Date of CSV', 'value':l.DateOfCSV.strftime("%m/%d/%Y") if l.DateOfCSV else ''})
    data.append({'label':'AV', 'value':"$%s" % intcomma(l.AV) if l.AV else ''})
    data.append({'label':'Date of AV', 'value':l.DateOfAV.strftime("%m/%d/%Y") if l.DateOfAV else ''})
    data.append({'label':'Loan Amount', 'value':'$%s' % intcomma(l.LoanAmount) if l.LoanAmount else ''})
    data.append({'label':'Loan Amount Date', 'value':l.LoanAmountDate.strftime("%m/%d/%Y") if l.LoanAmountDate else ''})
    data.append({'label':'Comments', 'value': l.Comments if l.Comments else ''})
    return output_html_table('Premium History Details', data)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def life_expectancy_table(request, id):
    l = models.LEAP2_LifeExpectancy.objects.get(LifeExpectancyID=id)
    data = []
    data.append({'label':'Report Date', 'value':  l.ReportDate.strftime("%m/%d/%Y") if l.ReportDate else ''})
    data.append({'label':'Provider', 'value':l.Provider})
    data.append({'label':'50% Months', 'value':l.LEMonths50})
    data.append({'label':'85% Months', 'value':l.LEMonths85})
    data.append({'label':'LE Mean', 'value':l.LEMean})
    data.append({'label':'LE Mortality', 'value':l.LEMortality})
    data.append({'label':'Primary Diagnosis', 'value':l.PrimaryDiagnosis})
    data.append({'label':'Smoking Class', 'value':l.SmokingClass})
    data.append({'label':'85% Months', 'value':l.LEMonths85})
    data.append({'label':'Terminal', 'value':l.Terminal})
    data.append({'label':'Med From Date', 'value':  l.MedFromDate.strftime("%m/%d/%Y") if l.MedFromDate else ''})
    data.append({'label':'Med To Date', 'value':  l.MedToDate.strftime("%m/%d/%Y") if l.MedToDate else ''})
    return output_html_table('Life Expectancy Report', data)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def policy_event_table(request, id):
    l = models.LEAP2_PolicyEvents.objects.get(PolicyEventID=id)
    data = []
    data.append({'label':'Type', 'value':l.EventType})
    data.append({'label':'Status', 'value':l.Status})
    data.append({'label':'Start Date', 'value':l.StartDate.strftime("%m/%d/%Y") if l.StartDate else ''})
    data.append({'label':'Complete Date', 'value':l.CompleteDate.strftime("%m/%d/%Y") if l.CompleteDate else ''})
    data.append({'label':'Job Number', 'value':l.JobNumber})
    data.append({'label':'Comments', 'value':l.Comments})
    return output_html_table('Insured Event Detail', data)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def insured_events_table(request, id):
    l = models.LEAP2_InsuredEvents.objects.get(InsuredEventID=id)
    if l.RequestDate:
        request_date = l.RequestDate.strftime("%m/%d/%Y")
    else:
        request_date = ''
    if l.ReceivedDate:
        received_date = l.ReceivedDate.strftime("%m/%d/%Y")
    else:
        received_date = ''
    data = []
    data.append({'label':'Type', 'value':l.EventType})
    data.append({'label':'Description', 'value':l.EventTypeDescription})
    data.append({'label':'Status', 'value':l.Status})
    data.append({'label':'Job Number', 'value':l.JobNumber})
    data.append({'label':'Request Date', 'value':request_date})
    data.append({'label':'Received Date', 'value': received_date})
    """
        Need to get the right notes field here.
        Commenting out for now
    """
    data.append({'label':'Notes', 'value':l.memo})
    return output_html_table('Insured Event Detail', data)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def policy_detail_table(request, policy_number):
    resp = "<div id='sample-table-sortable' class='panel'>"
    resp = "<table>"
    resp += "<thead>"
    resp += "<tr>"
    resp += "<th colspan='2'>Header Here</th>"
    resp += "</tr>"
    resp += "</thead>"
    resp += "<tbody>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "<tr>"
    resp += "<td>Name:</td><td>Rob Tucker</td>"
    resp += "</tr>"
    resp += "</tbody>"
    resp += "</table>"
    resp += "</div>"
    return HttpResponse(resp)

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def policy_detail(request, policy_number):
    try:
        policy = models.LEAP2_PolicyDetail.objects.filter(PolicyID=policy_number)[0]
    except:
        return HttpResponse('No Policy Found', 500)
    try:
        policy.Insured.SSN = '***-**-%s' % policy.Insured.SSN[7:12]
    except Exception, e:
        pass
    if str(policy.SubFundID) not in [str(r.SubFundID) for r in request.user.usersubfunds_set.all()]:
        return HttpResponseForbidden('You do not have access to this record')
    #premium_history = models.LEAP_PremiumHistoryInfo.objects.filter(PolicyNumber=policy.PolicyNumber).filter(PremiumReceived__gt=0).order_by('-DueDate')
    premium_history = models.LEAP2_PremiumInfo.objects.filter(PolicyNumber=policy.PolicyNumber).exclude(DueDate__gt=datetime.datetime.today()).order_by('-DueDate')
    """
        Updating to use new view
        #premiums_scheduled = models.LEAP2_PremiumInfo.objects.filter(PolicyNumber=policy.PolicyNumber).filter(DueDate__gt=datetime.datetime.today()).order_by('DueDate')
    """
    premiums_scheduled = models.LEAP2_PremiumInfoPending.objects.filter(PolicyNumber=policy.PolicyNumber).filter(DueDate__gt=datetime.datetime.today()).order_by('DueDate')
    #premiums_scheduled = models.LEAP_PremiumHistoryInfo.objects.filter(PolicyNumber=policy.PolicyNumber).filter(PremiumReceived=None).order_by('DueDate')[:6]
    policy_events = models.LEAP2_PolicyEvents.objects.filter(PolicyID=policy.PolicyID).order_by('StartDate')
    grace_events = models.LEAP2_GraceEvents.objects.filter(PolicyID=policy.PolicyID).order_by('-NoticeDate')
    maturity_claims = models.LEAP2_DeathClaim.objects.filter(PolicyID=policy.PolicyID)
    return render_to_response('leap/policy_detail.html', {
        'funds': get_fund_list(request),
        'policy':policy,
        'premium_history': premium_history,
        'active_page':'policy_detail',
        'premiums_scheduled': premiums_scheduled,
        'grace_events': grace_events,
        'policy_events': policy_events,
        'maturity_claims': maturity_claims,
        'page_title': 'Policy Detail - %s' % (policy.FunderPolicyID),
        }, RequestContext(request) )

def le_report_by_age_sex_data(request):
    policy_id = '260573'
    ticks = [
            'No LE',
            '48+',
            '36-47',
            '24-35',
            '12-23',
            '0-12',
            ]

    men_x = []
    for t in ticks:
        men_x.append(t)
    men_y = []
    for i in range(0,6):
        men_y.append(0)

    women_x = []
    for t in ticks:
        women_x.append(t)
    women_y = []
    for i in range(0,6):
        women_y.append(0)
    policy_list = get_policy_list(request)

    records = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
    #records = models.LEAP2_Insured_from_PC.objects.defer('PolicyID').defer('FundID').defer('SubFundID').exclude(LatestLEAgeinMos='0-12').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')
    for r in records:
        if r.LatestLEAgeinMos == 'No LE Reports':
            r.LatestLEAgeinMos = 'No LE'
        if r.Gender == 'Male':
            men_y[men_x.index(r.LatestLEAgeinMos)] += 1
        if r.Gender == 'Female':
            women_y[women_x.index(r.LatestLEAgeinMos)] += 1
    """label: "Men",
    legendEntry: true,
    data: { x: ['Never', '48+', '36-47', '24-35', '12-23', '0-12'], y: [5, 3, 4, 7, 2, 9] }
    """
    final_response = []
    final_response.append({
            'label':"Male",
            'legendEntry': 'true',
            'data' : {
                'x': men_x,
                'y': men_y,
                }
            })
    final_response.append({
            'label':'Female',
            'legendEntry': 'true',
            'data' : {
                'x': women_x,
                'y': women_y,
                }
            })
    return final_response

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def le_report_by_age_sex(request):
    return HttpResponse(json.dumps(le_report_by_age_sex_data(request)))

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def age_distribution_chart(request):
    return HttpResponse(json.dumps(AGE_DISTRIBUTION_COLORS))

@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def premiums_chart(request): 
    return HttpResponse(json.dumps(calculate_premiums_chart(request)))

def calculate_premiums_chart_by_date(request, start_date, end_date):
    final_response = {}
    policy_list = get_policy_list(request)
    """
        Update to use the new model view per revisions -2
        premiums = models.LEAP2_PremiumInfo.objects.values('Year_Month').filter(SubFundID__in=policy_list).filter(Q(DueDate__gte=start_date) & Q(DueDate__lte=end_date)).annotate(Sum('PremiumDue'))[0:12]
    """
    premiums = models.LEAP2_PremiumInfoPending.objects.values('Year_Month').filter(SubFundID__in=policy_list).filter(Q(DueDate__gte=start_date) & Q(DueDate__lte=end_date) & Q(PolicyStatus__contains='Active')).annotate(Sum('TotalDue'))[0:12]
    response = []
    for p in premiums:
        ym = str(p['Year_Month'])
        tmp = ym.split("-")
        ym = "%s/%s/%s" % (tmp[1],1,tmp[0])
        ym = ym.lstrip("0")
        response.append({'date': ym, 'value': int(p['TotalDue__sum'])})
    final_response["premiums"] = response
    
    return final_response

def calculate_premiums_chart(request):
    today = datetime.datetime.now().strftime("%Y-%m")
    policy_list = get_policy_list(request)
    """
        Original Premiums list before switching to new model
        premiums = models.LEAP2_PremiumInfo.objects.values('Year_Month').filter(SubFundID__in=policy_list).filter(Year_Month__gt=today).order_by('Year_Month').annotate(Sum('PremiumDue'))[0:8]
    """
    premiums = models.LEAP2_PremiumInfoPending.objects.values('Year_Month').filter(SubFundID__in=policy_list).filter(Year_Month__gt=today).filter(PolicyStatus__contains='Active').order_by('Year_Month').annotate(Sum('TotalDue'))[0:8]
    final_response = {}
    response = []
    for p in premiums:
        ym = str(p['Year_Month'])[2:]
        tmp = ym.split("-")
        ym = "%s-%s" % (tmp[1],tmp[0])
        ym = ym.lstrip("0")
        response.append({'date': ym, 'value': int(p['TotalDue__sum'])})
    final_response["premiums"] = response
    
    return final_response
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def gender_age_breakdown_chart(request):
    return HttpResponse(json.dumps(gender_age_breakdown_chart_data(request)))

def gender_age_breakdown_data(request):
    policy_list = get_policy_list(request)
    premiums = models.LEAP2_PolicyDetail.objects.values('GenderAgeGroup').filter(SubFundID__in=policy_list).annotate(Sum('CurrentNDB'))
    policy_counts = models.LEAP2_PolicyDetail.objects.values('GenderAgeGroup').filter(SubFundID__in=policy_list).annotate(Count('PolicyID'))
    total_amount = 0
    data_response = []
    counter = 1
    counted_total = 0
    for p in premiums:
        total_amount += int(p['CurrentNDB__sum'])
    for p in premiums:
        color = '#000000'
        for c in AGE_DISTRIBUTION_COLORS:  
            if str(c['label']) == str(p['GenderAgeGroup']):
                color = c['color']
        policy_count = 0;
        for pc in policy_counts:
            if str(pc['GenderAgeGroup']) == str(p['GenderAgeGroup']):
                policy_count = int(pc['PolicyID__count'])
        data_response.append(
                {
                    'label':p['GenderAgeGroup'], 
                    'data': int(p['CurrentNDB__sum']),
                    'color': color,
                    'count': policy_count,
                    'percent': "%.2f" % ((int(p['CurrentNDB__sum']) / total_amount) * 100),
                }
            )
        counted_total += int(p['CurrentNDB__sum'])
    return data_response
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def remaining_le_distribution_chart(request):
    return HttpResponse(json.dumps(remaining_le_distribution_chart_data(request)))

def insurance_company_distribution_data(request, distinct_len=10):
    policy_list = get_policy_list(request)
    premiums = models.LEAP2_PolicyDetail.objects.values('InsuranceCompanyName', 'InsCoID').filter(SubFundID__in=policy_list).annotate(Sum('CurrentNDB')).annotate(Count('PolicyID')).order_by('-CurrentNDB')
    holder = []
    for p in premiums:
        if not any(d.get('InsuranceCompanyName') == p['InsuranceCompanyName'] for d in holder):
            holder.append({'InsCoID': p['InsCoID'], 'InsuranceCompanyName': p['InsuranceCompanyName'],'PolicyID__count': int(p['PolicyID__count']), 'CurrentNDB__sum': int(p['CurrentNDB__sum'])})
        else:
            for h in holder:
                if h['InsuranceCompanyName'] == p['InsuranceCompanyName']:
                    try:
                        h['CurrentNDB__sum'] += int(p['CurrentNDB__sum'])
                    except:
                        pass
                    try:
                        h['PolicyID__count'] += int(p['PolicyID__count'])
                    except:
                        pass
                    continue

    premiums = holder
    #print len(premiums)
    #print premiums.query
    total_amount = 0
    data_response = []
    counter = 1
    counted_total = 0
    policy_count_total = 0
    policy_count = 0
    total_amount = 0
    if distinct_len == -1:
        distinct_len = len(premiums)
    for p in premiums:
        total_amount += int(p['CurrentNDB__sum'])
        policy_count_total += int(p['PolicyID__count'])
    style_counter = 0
    for p in premiums:
        if counter < distinct_len:
            percent = ((int(p['CurrentNDB__sum']) / total_amount) * 100)
            data_response.append(
                    {
                        'label': '%s - %.2f%%' % (p['InsuranceCompanyName'], percent),
                        'label_name_only':p['InsuranceCompanyName'], 
                        'InsCoID':p['InsCoID'], 
                        'data': int(p['CurrentNDB__sum']),
                        'color': GENERATED_SERIES_STYLES[style_counter]['fill'],
                        'percent': "%.2f" % percent,
                        'policy_count': "%i" % (int(p['PolicyID__count'])),
                    }
                )
            counted_total += int(p['CurrentNDB__sum'])
            policy_count += int(p['PolicyID__count'])
        if counter == distinct_len:
            percent = (((total_amount - counted_total) / total_amount) * 100)
            data_response.append(
                    {
                        'label':'ALL OTHERS - %.2f%%' % percent, 
                        'label_name_only':'ALL OTHERS', 
                        'data': (total_amount - counted_total),
                        'color': GENERATED_SERIES_STYLES[style_counter]['fill'],
                        'policy_count': "%i" % (policy_count_total - policy_count),
                        'percent': "%.2f" % (((total_amount - counted_total) / total_amount) * 100),
                    }
                )
            break
        style_counter += 1
        if style_counter == len(GENERATED_SERIES_STYLES):
            style_counter = 0
        counter += 1
    data_response = sorted(data_response, key=lambda k: float(k['percent'])) 
    data_response.reverse()
    return data_response
def remaining_le_distribution_chart_data(request):
    policy_list = get_policy_list(request)
    premiums = models.LEAP2_PolicyDetail.objects.values('Gender', 'LERemainingGroup').exclude(LERemainingGroup='No LE on File').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active - In-Force')).annotate(Sum('CurrentNDB')).annotate(Count('PolicyID')).order_by('LERemainingGroup')
    men_response = []
    women_response = []
    final_men_response = []
    final_women_response = []
    genders = [

            'Male',
            'Female',
            ]
    order = [
                str('<0'),
                str('0-3'),
                str('4-7'),
                str('8-11'),
                str('12-15'),
                str('15+'),

            ]
    for o in order:
        men_response.append({'date': o, 'value': 0})
        women_response.append({'date': o, 'value': 0})
    for p in premiums:
        p['LERemainingGroup'] = p['LERemainingGroup'].replace(" years", "")
        if p['Gender'] == 'Male':
            for m in men_response:
                if m['date'] == str(p['LERemainingGroup']):
                    m['value'] = int(p['CurrentNDB__sum'])
                    m['count'] = int(p['PolicyID__count'])
        if p['Gender'] == 'Female':
            for m in women_response:
                if m['date'] == str(p['LERemainingGroup']):
                    m['value'] = int(p['CurrentNDB__sum'])
                    m['count'] = int(p['PolicyID__count'])
            #man_response['date':str(p['LERemainingGroup']), 'value': int(p['CurrentNDB__sum'])});
        elif p['Gender'] == 'Female':
            women_response.append({'date':str(p['LERemainingGroup']), 'value': int(p['CurrentNDB__sum'])});
    final_response = {} 
    final_response['male'] = men_response
    final_response['female'] = women_response

    return final_response
@login_required
@user_passes_test(lambda u: u.groups.filter(name='leap').count() == 1, login_url='/login/')
def get_fund_list(request):
    fundlist = []
    subfundlist = []
    unique_fund_list = []
    final_fund_list = []
    ## Loop through all of the subfund objects tied to a user
    for s in request.user.usersubfunds_set.all():
        #subfund = models.LEAP_SubFundSummary.objects.get(SubFundID=s.SubFundID)
        subfund = models.LEAP2_SubFund.objects.get(SubFundID=s.SubFundID)
        fundlist.append(subfund)
    # Add all of the unique fund names to a list
    for f in fundlist:
        if f.FundName.strip() not in unique_fund_list:
            unique_fund_list.append(f.FundName.strip())
    # Create dictionary objects for each unique fund
    for f in unique_fund_list:
        final_fund_list.append({'fund_name': f, 'sub_funds':[]})
    # Iterate over the subfunds and add to dictionary object if it doesn't exist
    for f in fundlist:
        for fl in final_fund_list:
            if fl['fund_name'] == f.FundName.strip():
                fl['sub_funds'].append({'sub_fund_id': str(f.SubFundID), 'sub_fund_name': f.SubFundName.strip()})
    return final_fund_list

@login_required
@csrf_exempt
def set_session(request):
    try:
        funds = request.POST.getlist("funds[]")
        tmp = []
        for fund in funds:
            tmp.append(str(fund))
        request.session['selected_funds'] = tmp
            
    except Exception, e:
        pass
    return HttpResponse('OK')

def get_policy_list(request):
    policyList = []
    try:    
        if len(request.session['selected_funds']) > 0:
            policyList = request.session['selected_funds']
        else:
            for p in request.user.usersubfunds_set.all():
                policyList.append(p.SubFundID)
    except:
        for p in request.user.usersubfunds_set.all():
            policyList.append(p.SubFundID)
    return policyList

@csrf_exempt
def xls_simple(request, data, filename):
    from excel_response import ExcelResponse
    return ExcelResponse(data, filename)


def xls_to_response(xls, fname):
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s' % fname
    xls.save(response)
    return response
def currency(dollars):
    dollars = round(float(dollars), 2)
    return "$%s%s" % (intcomma(int(dollars)), ("%0.2f" % dollars)[-3:])

