from django.db import models
from django.db.models.query import QuerySet
from django.db.models import Q
from django.core.urlresolvers import reverse
from local_app.models import ReadAlerts
import caching.base

class CustomQuerySetManager(models.Manager):
    def __getattr__(self, attr, *args):
        if attr.startswith("_"): # or at least "__"
            raise AttributeError
        return getattr(self.get_query_set(), attr, *args)

    def get_query_set(self):
        return self.model.QuerySet(self.model)

class LEAP_Policy(caching.base.CachingMixin, models.Model):
    FundName = models.CharField(max_length=30, blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True, primary_key=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    FullNameLastFirst_Primary = models.CharField(max_length=70, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    PolicyDate = models.DateTimeField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    CurrentDeathBenefit = models.IntegerField(blank=True, null=True)
    ClosingCompleteDate = models.DateTimeField(blank=True, null=True)
    BenefitType = models.CharField(max_length=10, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    InsuredID_Primary = models.IntegerField(blank=True, null=True)
    InsuredID_Secondary = models.IntegerField(blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP_Policy'

class LEAP_SubFund(models.Model):
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundTaxID = models.CharField(max_length=11, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    Address1 = models.CharField(max_length=50, blank=True, null=True)
    Address2 = models.CharField(max_length=50, blank=True, null=True)
    City = models.CharField(max_length=20, blank=True, null=True)
    State = models.CharField(max_length=2, blank=True, null=True)
    ZipCode = models.CharField(max_length=10, blank=True, null=True)
    Country = models.CharField(max_length=35, blank=True, null=True)
    CorporatePhoneNumber = models.CharField(max_length=19, blank=True, null=True)
    CorporateWebsite = models.CharField(max_length=50, blank=True, null=True)
    PhoneNumber = models.CharField(max_length=12, blank=True, null=True)
    SubFundStatus = models.CharField(max_length=10, blank=True, null=True)
    LinkFunder = models.IntegerField(blank=True, null=True)
    LinkPremiumPayor = models.IntegerField(blank=True, null=True)
    ContractDate = models.DateTimeField(blank=True, null=True)
    PrimaryKeyToFunder = models.CharField(max_length=25, blank=True, null=True)
    #Fund = models.CharField(max_length=25, blank=True, null=True, db_column = 'objectID', to_field='objectID')
    objectID = models.IntegerField(blank=True, null=True, primary_key=True)

    def __unicode__(self):
        return str(self.objectID)

    class Meta:
        managed = False
        db_table = 'LE_SubFund_1180'


class LEAP_PolicyComplete(models.Model):
    PolicyNumber = models.CharField(max_length=16, blank=False, null=False)
    CurrentDeathBenefit = models.IntegerField(blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    IssueDate = models.DateTimeField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    PremiumAmount = models.IntegerField(blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    SSN = models.CharField(max_length=11, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    CCMonitoringStatus = models.CharField(max_length=10, blank=True, null=True)
    CCComments = models.IntegerField(blank=True, null=True)
    MoodyRating = models.CharField(max_length=25, blank=True, null=True)
    AMBestRating = models.CharField(max_length=4, blank=True, null=True)
    SPRating = models.CharField(max_length=4, blank=True, null=True)
    DeathClaimPhone1 = models.CharField(max_length=12, blank=True, null=True)
    DeathClaimPhone2 = models.CharField(max_length=12, blank=True, null=True)
    CustomerServicePhone1 = models.CharField(max_length=12, blank=True, null=True)
    CustomerServicePhone2 = models.CharField(max_length=12, blank=True, null=True)
    Website = models.CharField(max_length=50, blank=True, null=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    ClosingCompleteDate = models.DateTimeField(blank=True, null=True)
    AnnualStatementYear = models.IntegerField(blank=True, null=True)
    BenefitType = models.CharField(max_length=10, blank=True, null=True)
    IllustrationYear = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True, primary_key=True)
    #SubFundID = models.IntegerField(blank=True, null=True)
    SubFundID = models.ForeignKey('LEAP_SubFund', null=True, blank=True, db_column = 'SubFundID', to_field='objectID')
    FundID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    #InsuredID = models.IntegerField(blank=True, null=True)
    InsuredID_Primary = models.ForeignKey('LEAP_Insured', null=True, blank=True, db_column = 'InsuredID', to_field='InsuredID')
    FunderInsuredID = models.CharField(max_length=30, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    InsuredID_Secondary = models.ForeignKey('LEAP_Insured', null=True, blank=True, db_column = 'InsuredID', to_field='InsuredID', related_name='secondary')
    #InsuredID_Secondary = models.IntegerField(blank=True, null=True)
    #InsuredID_Secondary = models.ForeignKey(to='LEAP_Insured', to_field='InsuredID')
    ReviewNextActionDate = models.DateTimeField(blank=True, null=True)
    PolicyDate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP_PolicyComplete'

class LEAP_Insured(models.Model):
    InsuredID = models.IntegerField(blank=True, null=True, primary_key=True, unique=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    InsuredStatus = models.CharField(max_length=25, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    SSN = models.CharField(max_length=11, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    DOD = models.DateTimeField(blank=True, null=True)
    SpouseName = models.CharField(max_length=50, blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    MaritalStatus = models.CharField(max_length=10, blank=True, null=True)
    NextActionDate = models.DateTimeField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    FunderInsuredID = models.CharField(max_length=30, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    NumPolicies = models.IntegerField(blank=True, null=True)
    AVSReportDate = models.DateTimeField(blank=True, null=True)
    AVS50pctMonths = models.CharField(max_length=3, blank=True, null=True)
    AVS85pctMonths = models.CharField(max_length=3, blank=True, null=True)
    AVSMortality = models.CharField(max_length=5, blank=True, null=True)
    FASANOReportDate = models.DateTimeField(blank=True, null=True)
    FASANO50pctMonths = models.CharField(max_length=3, blank=True, null=True)
    FASANO85pctMonths = models.CharField(max_length=3, blank=True, null=True)
    FASANOMortality = models.CharField(max_length=5, blank=True, null=True)
    date21stReportDate = models.DateTimeField(db_column='21stReportDate', blank=True, null=True)
    date21st50pctMonths = models.CharField(db_column='21st50pctMonths', max_length=3, blank=True, null=True)
    date21st85pctMonths = models.CharField(db_column='21st85pctMonths', max_length=3, blank=True, null=True)
    date21stMortality = models.CharField(db_column='21stMortality', max_length=5, blank=True, null=True)
    ISCReportDate = models.DateTimeField(blank=True, null=True)
    ISC50pctMonths = models.CharField(max_length=3, blank=True, null=True)
    ISC85pctMonths = models.CharField(max_length=3, blank=True, null=True)
    ISCMortality = models.CharField(max_length=5, blank=True, null=True)
    EMSIReportDate = models.DateTimeField(blank=True, null=True)
    EMSI50pctMonths = models.CharField(max_length=3, blank=True, null=True)
    EMSI85pctMonths = models.CharField(max_length=3, blank=True, null=True)
    EMSIMortality = models.CharField(max_length=5, blank=True, null=True)
    ConfirmedAliveBySSN = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP_Insured'
class LEAP_PremiumHistoryInfo(caching.base.CachingMixin, models.Model):
    Year_Month = models.CharField(db_column='Year-Month', max_length=7, blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    DueDate = models.DateTimeField(blank=True, null=True)
    PremiumDue = models.IntegerField(blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    Payee = models.CharField(max_length=100, blank=True, null=True)
    PremiumReceivedDate = models.DateTimeField(blank=True, null=True)
    PremiumReceived = models.IntegerField(blank=True, null=True)
    AV = models.IntegerField(blank=True, null=True)
    DateOfAV = models.DateTimeField(blank=True, null=True)
    CSV = models.IntegerField(blank=True, null=True)
    DateOfCSV = models.DateTimeField(blank=True, null=True)
    InGrace = models.CharField(max_length=3, blank=True, null=True)
    PremConfMethod = models.CharField(max_length=25, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    PayeeID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    ProductID = models.IntegerField(blank=True, null=True)
    InsuredID_Primary = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PremiumID = models.IntegerField(blank=True, null=True, primary_key=True)
    Comments = models.TextField(max_length=2147483647, blank=True, null=True)
    LoanAmountDate = models.DateTimeField(blank=True, null=True)
    LoanAmount = models.IntegerField(blank=True, null=True)
    LoanPaymentDueDate = models.DateTimeField(blank=True, null=True)
    LoanPaymentAmount = models.IntegerField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)

    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP_PremiumHistoryInfo'

class LEAP_PolicyEvents(models.Model):
    PolicyEventID = models.IntegerField(blank=True, null=True)
    EventType = models.CharField(max_length=30, blank=True, null=True)
    Status = models.CharField(max_length=9, blank=True, null=True)
    StartDate = models.DateTimeField(blank=True, null=True)
    CompleteDate = models.DateTimeField(blank=True, null=True)
    Comments = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    JobID = models.IntegerField(blank=True, null=True)
    AssignedTo = models.CharField(max_length=50, blank=True, null=True)
    FollowUpDate = models.DateTimeField(blank=True, null=True)
    PolicyEventMonth = models.CharField(max_length=10, blank=True, null=True)
    PolicyEventMonthNumeric = models.CharField(max_length=2, blank=True, null=True)
    AmountOfFundsReceived = models.IntegerField(blank=True, null=True)
    INSStartDate = models.DateTimeField(blank=True, null=True)
    INSFollowUpDate = models.DateTimeField(blank=True, null=True)
    INSCompleteDate = models.DateTimeField(blank=True, null=True)
    FunderStartDate = models.DateTimeField(blank=True, null=True)
    FunderFollowUpDate = models.DateTimeField(blank=True, null=True)
    FunderCompleteDate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP_PolicyEvents'
class LEAP_SubFundSummary(models.Model):
    FundName = models.CharField(max_length=30, blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True, primary_key=True)
    InsuredCount = models.IntegerField(blank=True, null=True)
    PolicyCount = models.IntegerField(blank=True, null=True)
    PolicyCountActive = models.IntegerField(blank=True, null=True)
    PolicyCountDeathClaimPending = models.IntegerField(blank=True, null=True)
    PolicyCountLapsePending = models.IntegerField(blank=True, null=True)
    PolicyCountMatured = models.IntegerField(blank=True, null=True)
    PolicyMaturedAmount = models.IntegerField(blank=True, null=True)
    PolicyDeathClaimAmount = models.IntegerField(blank=True, null=True)
    TotalPurchasedDB = models.IntegerField(blank=True, null=True)
    MinPurchasedDB = models.IntegerField(blank=True, null=True)
    MaxPurchasedDB = models.IntegerField(blank=True, null=True)
    AvgPurchasedDB = models.IntegerField(blank=True, null=True)
    Carriers = models.IntegerField(blank=True, null=True)
    PremiumNext30days = models.IntegerField(blank=True, null=True)
    PremiumNext60days = models.IntegerField(blank=True, null=True)
    PremiumNext90days = models.IntegerField(blank=True, null=True)
    PremiumNext120days = models.IntegerField(blank=True, null=True)
    PremiumThisCalYear = models.IntegerField(blank=True, null=True)
    PremiumNext365days = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP_SubFundSummary'
class LEAP_PolicyByLatestAge(models.Model):
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True, primary_key=True)
    FundID = models.IntegerField(blank=True, null=True)
    #PolicyID = models.IntegerField(blank=True, null=True)
    PolicyID = models.ForeignKey('LEAP_PolicyComplete', null=True, blank=True, db_column = 'PolicyID', to_field='PolicyID')
    CurrentNDB = models.IntegerField(blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    LatestLEAgeInMos = models.CharField(max_length=128, blank=True, null=True)
    Provider = models.CharField(max_length=20, blank=True, null=True)
    LEMonths50 = models.CharField(max_length=3, blank=True, null=True)
    LatestLEReportDate = models.DateTimeField(blank=True, null=True)
    AgeOfReport = models.IntegerField(blank=True, null=True)
    LERemaining = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP2_PolicybyLatestLEAge'

class LEAP2_Insured_from_PC(caching.base.CachingMixin, models.Model):
    InsuredID = models.IntegerField(blank=True, null=True, primary_key=True)
    #Insured = models.ForeignKey('LEAP_Insured',to_field='InsuredID', db_column='InsuredID')
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    FunderInsuredID = models.CharField(max_length=70, blank=True, null=True)
    InsuredStatus = models.CharField(max_length=25, blank=True, null=True)
    SSN = models.CharField(max_length=25, blank=True, null=True)
    InsuredState = models.CharField(max_length=2, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    AgeinYrs = models.IntegerField(blank=True, null=True)
    DOD = models.DateTimeField(blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    MaritalStatus = models.CharField(max_length=10, blank=True, null=True)
    SpouseName = models.CharField(max_length=50, blank=True, null=True)
    NextContact = models.DateTimeField(blank=True, null=True)
    LastContact = models.DateTimeField(blank=True, null=True)
    LatestLEAgeinMos = models.CharField(max_length=13, blank=True, null=True)
    AgeOfReport = models.IntegerField(blank=True, null=True)
    LERemaining = models.IntegerField(blank=True, null=True)
    LERemainingGroup = models.CharField(max_length=13, blank=True, null=True)
    ConfirmedAlivebySSN = models.DateTimeField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)

    @property
    def clear_ssn(self):
        return self.SSN

    #objects = CustomQuerySetManager()
    objects = caching.base.CachingManager()

    class QuerySet(QuerySet):
        def active_by_age_gender_in_policy_list(self, policy_age, policy_gender, policy_list, *args, **kwargs):
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(LatestLEAgeinMos=policy_age).filter(Gender=policy_gender).filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')

        def active_in_policy_list(self, policy_list, *args, **kwargs):
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active')

        def inactive_in_policy_list(self, policy_list, *args, **kwargs):
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(Q(InsuredStatus='Inactive') | Q(InsuredStatus='Deceased'))

        def outdated_in_policy_list(self, policy_list, *args, **kwargs):
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).distinct('InsuredID').filter(InsuredStatus='Active').filter(Q(AgeOfReport__gt=13) | Q(AgeOfReport=None)).order_by('-AgeOfReport')
    class Meta:
        managed = False
        db_table = 'LEAP2_Insured_from_PC'

class LEAP2_Policy_from_PC(caching.base.CachingMixin, models.Model):
    PolicyID = models.IntegerField(blank=True, null=True, primary_key=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    ## this is the sum by for x axis on remaining le distribution graph
    ## We do not have a way to get back to the insured gender
    ## Sum this for the insurance company breakdown, take top 5
    CurrentNDB = models.IntegerField(blank=True, null=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    ## Group by this for the insurance company breakdown
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    IssueDate = models.DateTimeField(blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    PolicyDate = models.DateTimeField(blank=True, null=True)
    PremiumAmount = models.IntegerField(blank=True, null=True)
    BenefitType = models.CharField(max_length=10, blank=True, null=True)
    ## this is the group by for x axis on remaining le distribution graph
    LERemainingGroup = models.CharField(max_length=13, blank=True, null=True)
    LERemaining = models.IntegerField(blank=True, null=True)
    ## make sure to group the insurance company breakdown by this
    SubFundID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    #objects = CustomQuerySetManager()
    objects = caching.base.CachingManager()

    @property
    def FullNameLastFirst(self):
        insured_id = LEAP_Policy.objects.get(PolicyID=self.PolicyID).InsuredID_Primary
        insured = LEAP2_Insured_from_PC.objects.filter(InsuredID=insured_id)[0]
        return insured.FullNameLastFirst
    
    @property
    def InsuredID(self):
        insured_id = LEAP_Policy.objects.get(PolicyID=self.PolicyID).InsuredID_Primary
        return insured_id

    class QuerySet(QuerySet):
        def active_in_policy_list_by_le_age_and_gender(self, policy_list, age, gender, *args, **kwargs):
            #return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active')).filter(LERemaining=age).filter(Gender=gender)
            ret = self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active')).filter(LERemainingGroup=age)#.filter(Gender=gender)
            return ret
            #return self.filter(SubFundID__in=policy_list)
        def active_in_policy_list(self, policy_list, *args, **kwargs):
            #return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(PolicyStatus='Active')
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active - In-Force'))
            #return self.filter(SubFundID__in=policy_list)

        def inactive_in_policy_list(self, policy_list, *args, **kwargs):
            return self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Death Claim Litigation') | Q(PolicyStatus='Inactive') | Q(PolicyStatus='Lapsed - Per Funder') | Q(PolicyStatus='Matured'))

    class Meta:
        managed = False
        db_table = 'LEAP2_Policy_from_PC'
class LEAP2_GraceEvents(models.Model):
    PolicyID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    GraceStatus = models.CharField(max_length=40, blank=True, null=True)
    NoticeDate = models.DateTimeField(blank=True, null=True)
    LapseDate = models.DateTimeField(blank=True, null=True)
    DateEnteredGrace = models.DateTimeField(blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    CurrentNDB = models.IntegerField(blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    Comments = models.TextField(max_length=2147483647, blank=True, null=True)
    TotalAmountDue = models.IntegerField(blank=True, null=True)
    PaidAmount = models.IntegerField(blank=True, null=True)
    PaidDate = models.DateTimeField(blank=True, null=True)
    GraceEventID = models.IntegerField(blank=True, null=True, primary_key=True)
    ConfirmedDate = models.DateTimeField(blank=True, null=True)
    AssignedTo = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP2_GraceEvents'
class LEAP2_LifeExpectancy(models.Model):
    LifeExpectancyID = models.IntegerField(blank=True, null=True, primary_key=True)
    ReportDate = models.DateTimeField(blank=True, null=True)
    Provider = models.CharField(max_length=20, blank=True, null=True)
    PrimaryDiagnosis = models.CharField(max_length=60, blank=True, null=True)
    SmokingClass = models.CharField(max_length=10, blank=True, null=True)
    Terminal = models.CharField(max_length=3, blank=True, null=True)
    FunderID = models.IntegerField(blank=True, null=True)
    LEMean = models.CharField(max_length=3, blank=True, null=True)
    #InsuredID = models.IntegerField(blank=True, null=True)
    InsuredID = models.ForeignKey('LEAP2_Insured_from_PC', null=True, blank=True, db_column = 'InsuredID', to_field='InsuredID')
    MedFromDate = models.DateTimeField(blank=True, null=True)
    MedToDate = models.DateTimeField(blank=True, null=True)
    LEMonths50 = models.CharField(max_length=3, blank=True, null=True)
    LEMortality = models.CharField(max_length=5, blank=True, null=True)
    LEMonths85 = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP2_LifeExpectancy_1182'

class LEAP2_SubFund(caching.base.CachingMixin, models.Model):
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    FundName = models.CharField(max_length=70, blank=True, null=True)
    SubFundTaxID = models.CharField(max_length=11, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    Address1 = models.CharField(max_length=50, blank=True, null=True)
    Address2 = models.CharField(max_length=50, blank=True, null=True)
    City = models.CharField(max_length=20, blank=True, null=True)
    State = models.CharField(max_length=2, blank=True, null=True)
    ZipCode = models.CharField(max_length=10, blank=True, null=True)
    Country = models.CharField(max_length=35, blank=True, null=True)
    CorporatePhoneNumber = models.CharField(max_length=19, blank=True, null=True)
    CorporateWebsite = models.CharField(max_length=50, blank=True, null=True)
    PhoneNumber = models.CharField(max_length=12, blank=True, null=True)
    SubFundStatus = models.CharField(max_length=10, blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PremiumPayorID = models.IntegerField(blank=True, null=True)
    ContractDate = models.DateTimeField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True, primary_key=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_SubFund'
class LEAP2_SubFund_1180(caching.base.CachingMixin, models.Model):
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundTaxID = models.CharField(max_length=11, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    Address1 = models.CharField(max_length=50, blank=True, null=True)
    Address2 = models.CharField(max_length=50, blank=True, null=True)
    City = models.CharField(max_length=20, blank=True, null=True)
    State = models.CharField(max_length=2, blank=True, null=True)
    ZipCode = models.CharField(max_length=10, blank=True, null=True)
    Country = models.CharField(max_length=35, blank=True, null=True)
    CorporatePhoneNumber = models.CharField(max_length=19, blank=True, null=True)
    CorporateWebsite = models.CharField(max_length=50, blank=True, null=True)
    PhoneNumber = models.CharField(max_length=12, blank=True, null=True)
    SubFundStatus = models.CharField(max_length=10, blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PremiumPayorID = models.IntegerField(blank=True, null=True)
    ContractDate = models.DateTimeField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True, primary_key=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_SubFund_1180'

"""class LEAP2_InsuredEvents(caching.base.CachingMixin, models.Model):
    InsuredEventID = models.IntegerField(blank=True, null=True, primary_key=True)
    EventType = models.CharField(max_length=35, blank=True, null=True)
    Status = models.CharField(max_length=15, blank=True, null=True)
    RequestDate = models.DateTimeField(blank=True, null=True)
    ReceivedDate = models.DateTimeField(blank=True, null=True)
    LastContact = models.DateTimeField(blank=True, null=True)
    NextContact = models.DateTimeField(blank=True, null=True)
    InternalComments = models.IntegerField(blank=True, null=True)
    ExternalComments = models.IntegerField(blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True)
    JobID = models.IntegerField(blank=True, null=True)
    CreatedBy = models.CharField(max_length=50, blank=True, null=True)
    FollowUpDate = models.DateTimeField(blank=True, null=True)
    InsServicingUser = models.CharField(max_length=15, blank=True, null=True)
    BilledDate = models.DateTimeField(blank=True, null=True)
    AmountToBill = models.IntegerField(blank=True, null=True)
    LifeEquityFee = models.IntegerField(blank=True, null=True)
    PaymentMethod = models.CharField(max_length=20, blank=True, null=True)
    EventTypeDescription = models.CharField(max_length=50, blank=True, null=True)
    memo = models.TextField(max_length=2147483647, blank=True, null=True)
    JobNumber = models.CharField(max_length=10, blank=True, null=True)
    FunderID = models.IntegerField(blank=True, null=True)
    objects = caching.base.CachingManager()
    @property
    def Insured(self):
        try:
            return LEAP2_Insured_from_PC.objects.filter(InsuredID=self.InsuredID)[0]
        except:
            return None

    class Meta:
        managed = False
        db_table = 'LEAP2_InsuredEvents'"""
class LEAP2_InsuredEvents(caching.base.CachingMixin, models.Model):
    InsuredEventID = models.IntegerField(blank=True, null=True, primary_key=True)
    EventType = models.CharField(max_length=35, blank=True, null=True)
    Status = models.CharField(max_length=15, blank=True, null=True)
    RequestDate = models.DateTimeField(blank=True, null=True)
    ReceivedDate = models.DateTimeField(blank=True, null=True)
    LastContact = models.DateTimeField(blank=True, null=True)
    NextContact = models.DateTimeField(blank=True, null=True)
    InternalComments = models.IntegerField(blank=True, null=True)
    ExternalComments = models.IntegerField(blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True)
    JobID = models.IntegerField(blank=True, null=True)
    CreatedBy = models.CharField(max_length=50, blank=True, null=True)
    FollowUpDate = models.DateTimeField(blank=True, null=True)
    InsServicingUser = models.CharField(max_length=15, blank=True, null=True)
    BilledDate = models.DateTimeField(blank=True, null=True)
    AmountToBill = models.IntegerField(blank=True, null=True)
    LifeEquityFee = models.IntegerField(blank=True, null=True)
    PaymentMethod = models.CharField(max_length=20, blank=True, null=True)
    EventTypeDescription = models.CharField(max_length=50, blank=True, null=True)
    memo = models.TextField(max_length=2147483647, blank=True, null=True)
    JobNumber = models.CharField(max_length=10, blank=True, null=True)
    FunderID = models.IntegerField(blank=True, null=True)
    Insured = models.CharField(max_length=70, blank=True, null=True)

    objects = caching.base.CachingManager()
    """@property
    def Insured(self):
        try:
            return LEAP2_Insured_from_PC.objects.filter(InsuredID=self.InsuredID)[0]
        except:
            return None"""

    class Meta:
        managed = False
        db_table = 'LEAP2_InsuredEvents'
class LEAP2_PolicyDetail(caching.base.CachingMixin, models.Model):
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True, primary_key=True)
    CurrentNDB = models.IntegerField(blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    IssueDate = models.DateTimeField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    PremiumAmount = models.IntegerField(blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    SSN = models.CharField(max_length=11, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    CCMonitoringStatus = models.CharField(max_length=10, blank=True, null=True)
    CCComments = models.IntegerField(blank=True, null=True)
    MoodyRating = models.CharField(max_length=25, blank=True, null=True)
    AMBestRating = models.CharField(max_length=4, blank=True, null=True)
    SPRating = models.CharField(max_length=4, blank=True, null=True)
    DeathClaimPhone1 = models.CharField(max_length=12, blank=True, null=True)
    DeathClaimPhone2 = models.CharField(max_length=12, blank=True, null=True)
    CustomerServicePhone1 = models.CharField(max_length=12, blank=True, null=True)
    CustomerServicePhone2 = models.CharField(max_length=12, blank=True, null=True)
    Website = models.CharField(max_length=50, blank=True, null=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    ClosingCompleteDate = models.DateTimeField(blank=True, null=True)
    AnnualStatementYear = models.IntegerField(blank=True, null=True)
    BenefitType = models.CharField(max_length=10, blank=True, null=True)
    IllustrationYear = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    AgeGroup = models.CharField(max_length=8, blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    FunderInsuredID = models.CharField(max_length=30, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    GenderAgeGroup = models.CharField(max_length=18, blank=True, null=True)
    InsuredID_Secondary = models.IntegerField(blank=True, null=True)
    ReviewNextActionDate = models.DateTimeField(blank=True, null=True)
    PolicyDate = models.DateTimeField(blank=True, null=True)
    AgeOfReport = models.IntegerField(blank=True, null=True)
    LERemaining = models.IntegerField(blank=True, null=True)
    LERemainingGroup = models.CharField(max_length=13, blank=True, null=True)
    LatestLEAgeinMos = models.CharField(max_length=13, blank=True, null=True)
    objects = caching.base.CachingManager()
    #objects = CustomQuerySetManager()
    @property
    def Insured(self):
        try:
            return LEAP2_Insured_from_PC.objects.filter(InsuredID=self.InsuredID)[0]
        except:
            return None

    @property
    def InsuredSecondary(self):
        try:
            return LEAP2_Insured_from_PC.objects.filter(InsuredID=self.InsuredID_Secondary)[0]
        except Exception:
            return None


    class QuerySet(QuerySet):
        def active_in_policy_list_by_le_age_and_gender(self, policy_list, age, gender, *args, **kwargs):
            ret = self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active - In-Force')| Q(PolicyStatus='Active')).filter(LERemainingGroup=age)#.filter(Gender=gender)
            return ret

        def active_in_policy_list(self, policy_list, *args, **kwargs):
            ret = self.defer('PolicyID').defer('FundID').defer('SubFundID').filter(SubFundID__in=policy_list).filter(Q(PolicyStatus='Active - Lapse Pending') | Q(PolicyStatus='Active - Litigation') | Q(PolicyStatus='Death Claim Pending') | Q(PolicyStatus='Active - In-Force')| Q(PolicyStatus='Active - In-Force'))
            return ret
    def __unicode__(self):
        return self.PolicyNumber

    class Meta:
        managed = False
        db_table = 'LEAP2_PolicyDetail'

class LEAP2_Alerts(caching.base.CachingMixin, models.Model):
    FundID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    AlertDestinationID = models.IntegerField(blank=True, null=True)
    UniqueID = models.IntegerField(blank=True, null=True, primary_key=True)
    AlertDateTime = models.DateTimeField(blank=True, null=True)
    AlertType = models.CharField(max_length=11, blank=True, null=True)
    AlertString = models.CharField(max_length=659, blank=True, null=True)
    AlertLinkText = models.CharField(max_length=38, blank=True, null=True)
    DestinationType = models.CharField(max_length=1, blank=True, null=True)
    objects = caching.base.CachingManager()
    
    @property
    def alert_link(self):
        #if self.AlertType == 'Grace Event':
        #    return reverse('ack_alert_by_type', kwargs={'type': 'policy', 'id': self.AlertDetinationID, 'alert_id': self.UniqueID})
        #elif self.AlertType == 'LE Change':
        #    return reverse('ack_alert_by_type', kwargs={'type': 'insured', 'id': self.AlertDetinationID, 'alert_id': self.UniqueID})
        if self.DestinationType == 'P':
            return reverse('ack_alert_by_type', kwargs={'type': 'policy', 'id': self.AlertDestinationID, 'alert_id': self.UniqueID})
        elif self.DestinationType == 'I':
            return reverse('ack_alert_by_type', kwargs={'type': 'insured', 'id': self.AlertDestinationID, 'alert_id': self.UniqueID})
    @property
    def is_read(self):
        try:
            the_count = ReadAlerts.objects.get(AlertID=self.UniqueID)
            if the_count > 0:
                return True
            else:
                return False
        except Exception, e:
            return False
    class Meta:
        managed = False
        db_table = 'LEAP2_Alerts'

class LEAP2_DeathClaim(caching.base.CachingMixin, models.Model):
    PolicyID = models.IntegerField(blank=True, null=True, primary_key=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    SSN = models.CharField(max_length=11, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    CurrentNDB = models.IntegerField(blank=True, null=True)
    DBAmountReceived = models.IntegerField(blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    IssueDate = models.DateTimeField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PremiumAmount = models.IntegerField(blank=True, null=True)
    InsuredStatus = models.CharField(max_length=25, blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True)
    DeathBenefitClaimsFiled = models.CharField(max_length=3, blank=True, null=True)
    DenialofDeathBenefitClaims = models.CharField(max_length=3, blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    DeathNotificationMethod = models.CharField(max_length=25, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    DOD = models.DateTimeField(blank=True, null=True)
    DateNotifiedDeath = models.DateTimeField(blank=True, null=True)
    DateDBClaimFiled = models.DateTimeField(blank=True, null=True)
    PaymentRecd = models.DateTimeField(blank=True, null=True)
    AgeAtDeath = models.IntegerField(blank=True, null=True)
    DaysFromDeathToNotice = models.IntegerField(blank=True, null=True)
    DaysForDeathCert = models.IntegerField(blank=True, null=True)
    DaysfromDeathToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysfromDeathToDeathCertRecd = models.IntegerField(blank=True, null=True)
    DaysfromDeathCertToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysfromDeathToClaimPaid = models.IntegerField(blank=True, null=True)
    DaysNotifyToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysClaimFiledToPaid = models.IntegerField(blank=True, null=True)
    DaysToStartDeathCert = models.IntegerField(blank=True, null=True)
    DCStatus = models.CharField(max_length=25, blank=True, null=True)
    DeathCertStartDate = models.DateTimeField(blank=True, null=True)
    DeathCertCompleteDate = models.DateTimeField(blank=True, null=True)
    DeathCertStatus = models.CharField(max_length=15, blank=True, null=True)
    DeathCertMemo = models.TextField(max_length=2147483647, blank=True, null=True)
    DeathClaimStartDate = models.DateTimeField(blank=True, null=True)
    DeathClaimCompleteDate = models.DateTimeField(blank=True, null=True)
    DeathClaimEventStatus = models.CharField(max_length=15, blank=True, null=True)
    DeathClaimMemo = models.TextField(max_length=2147483647, blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_DeathClaim'

class LEAP2_LifeExpectanciesWithInsured(caching.base.CachingMixin, models.Model):
    FunderID = models.IntegerField(blank=True, null=True)
    LifeExpectancyID = models.IntegerField(blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True, primary_key=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    FunderInsuredID = models.CharField(max_length=30, blank=True, null=True)
    ReportDate = models.DateTimeField(blank=True, null=True)
    Provider = models.CharField(max_length=20, blank=True, null=True)
    LEMonths50 = models.CharField(max_length=3, blank=True, null=True)
    LEMonths85 = models.CharField(max_length=3, blank=True, null=True)
    LEMean = models.CharField(max_length=3, blank=True, null=True)
    LEMortality = models.CharField(max_length=5, blank=True, null=True)
    PrimaryDiagnosis = models.CharField(max_length=60, blank=True, null=True)
    SmokingClass = models.CharField(max_length=10, blank=True, null=True)
    Terminal = models.CharField(max_length=3, blank=True, null=True)
    MedFromDate = models.DateTimeField(blank=True, null=True)
    MedToDate = models.DateTimeField(blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_LifeExpectanciesWithInsured'
class LEAP2_Jobs(caching.base.CachingMixin, models.Model):
    JobMemoID = models.IntegerField(blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    JobNumber = models.CharField(max_length=10, blank=True, null=True)
    JobStatus = models.CharField(max_length=10, blank=True, null=True)
    JobRequestReceived = models.DateTimeField(blank=True, null=True)
    JobComplete = models.DateTimeField(blank=True, null=True)
    JobRequestedBy = models.CharField(max_length=40, blank=True, null=True)
    JobContacts = models.CharField(max_length=40, blank=True, null=True)
    memo = models.TextField(max_length=2147483647, blank=True, null=True)
    TotalEvents = models.IntegerField(blank=True, null=True)
    PendingEvents = models.IntegerField(blank=True, null=True)
    CompletedEvents = models.IntegerField(blank=True, null=True)
    JobID = models.IntegerField(blank=True, null=True, primary_key=True)
    FunderID = models.IntegerField(blank=True, null=True)
    JobAuthorizationDate = models.DateTimeField(blank=True, null=True)
    JobStartDate = models.DateTimeField(blank=True, null=True)
    TargetCompleteDate = models.DateTimeField(blank=True, null=True)
    JobManager = models.CharField(max_length=30, blank=True, null=True)
    EventType = models.CharField(max_length=40, blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_Jobs'
class LEAP2_PolicyEvents(caching.base.CachingMixin, models.Model):
    SubFundID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    PolicyEventID = models.IntegerField(blank=True, null=True)
    EventType = models.CharField(max_length=30, blank=True, null=True)
    Status = models.CharField(max_length=9, blank=True, null=True)
    StartDate = models.DateTimeField(blank=True, null=True)
    FollowUpDate = models.DateTimeField(blank=True, null=True)
    CompleteDate = models.DateTimeField(blank=True, null=True)
    JobID = models.IntegerField(blank=True, null=True, primary_key=True)
    AssignedTo = models.CharField(max_length=50, blank=True, null=True)
    JobNumber = models.CharField(max_length=10, blank=True, null=True)
    Comments = models.TextField(max_length=2147483647, blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_PolicyEvents'

class LEAP2_InsCos(caching.base.CachingMixin, models.Model):
    InsCoID = models.IntegerField(blank=True, null=True, primary_key=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    Address1 = models.CharField(max_length=50, blank=True, null=True)
    Address2 = models.CharField(max_length=50, blank=True, null=True)
    City = models.CharField(max_length=20, blank=True, null=True)
    State = models.CharField(max_length=2, blank=True, null=True)
    ZipCode = models.CharField(max_length=10, blank=True, null=True)
    PhoneNumber = models.CharField(max_length=12, blank=True, null=True)
    Website = models.CharField(max_length=50, blank=True, null=True)
    OvernightAddress1 = models.CharField(max_length=50, blank=True, null=True)
    OvernightAddress2 = models.CharField(max_length=50, blank=True, null=True)
    OvernightCity = models.CharField(max_length=20, blank=True, null=True)
    OvernightState = models.CharField(max_length=2, blank=True, null=True)
    OvernightZipCode = models.CharField(max_length=10, blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_InsCos'
class LEAP2_DeathClaim(caching.base.CachingMixin, models.Model):
    PolicyID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    SSN = models.CharField(max_length=11, blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    Gender = models.CharField(max_length=7, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    PurchasedDB = models.IntegerField(blank=True, null=True)
    CurrentNDB = models.IntegerField(blank=True, null=True)
    DBAmountReceived = models.IntegerField(blank=True, null=True)
    PolicyType = models.CharField(max_length=10, blank=True, null=True)
    PolicyState = models.CharField(max_length=2, blank=True, null=True)
    IssueDate = models.DateTimeField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PremiumAmount = models.IntegerField(blank=True, null=True)
    InsuredStatus = models.CharField(max_length=25, blank=True, null=True)
    InsuredID = models.IntegerField(blank=True, null=True)
    DeathBenefitClaimsFiled = models.CharField(max_length=3, blank=True, null=True)
    DenialofDeathBenefitClaims = models.CharField(max_length=3, blank=True, null=True)
    ProductName = models.CharField(max_length=50, blank=True, null=True)
    DeathNotificationMethod = models.CharField(max_length=25, blank=True, null=True)
    DOB = models.DateTimeField(blank=True, null=True)
    DOD = models.DateTimeField(blank=True, null=True)
    DateNotifiedDeath = models.DateTimeField(blank=True, null=True)
    DateDBClaimFiled = models.DateTimeField(blank=True, null=True)
    PaymentRecd = models.DateTimeField(blank=True, null=True)
    AgeAtDeath = models.IntegerField(blank=True, null=True)
    DaysFromDeathToNotice = models.IntegerField(blank=True, null=True)
    DaysForDeathCert = models.IntegerField(blank=True, null=True)
    DaysfromDeathToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysfromDeathToDeathCertRecd = models.IntegerField(blank=True, null=True)
    DaysfromDeathCertToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysfromDeathToClaimPaid = models.IntegerField(blank=True, null=True)
    DaysNotifyToClaimFiled = models.IntegerField(blank=True, null=True)
    DaysClaimFiledToPaid = models.IntegerField(blank=True, null=True)
    DaysToStartDeathCert = models.IntegerField(blank=True, null=True)
    DCStatus = models.CharField(max_length=25, blank=True, null=True)
    DeathCertStartDate = models.DateTimeField(blank=True, null=True)
    DeathCertCompleteDate = models.DateTimeField(blank=True, null=True)
    DeathCertStatus = models.CharField(max_length=15, blank=True, null=True)
    DeathCertMemo = models.TextField(max_length=2147483647, blank=True, null=True)
    DeathClaimStartDate = models.DateTimeField(blank=True, null=True)
    DeathClaimCompleteDate = models.DateTimeField(blank=True, null=True)
    DeathClaimEventStatus = models.CharField(max_length=15, blank=True, null=True)
    DeathClaimMemo = models.TextField(max_length=2147483647, blank=True, null=True)
    objects = caching.base.CachingManager()


    class Meta:
        managed = False
        db_table = 'LEAP2_DeathClaim'

class LEAP2_Credentials(models.Model):
    Username = models.CharField(max_length=50, blank=True, null=True, primary_key=True)
    Password = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP2_Credentials'
class LEAP2_MonthlyValuations(models.Model):
    FundID = models.IntegerField(blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    PolicyStatus = models.CharField(max_length=25, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True, primary_key=True)
    CurrentNDB = models.IntegerField(blank=True, null=True)
    AV = models.IntegerField(blank=True, null=True)
    CSV = models.IntegerField(blank=True, null=True)
    DateOfAVCSV = models.DateTimeField(blank=True, null=True)
    PremiumDue = models.IntegerField(blank=True, null=True)
    LastCOIDeduction = models.IntegerField(blank=True, null=True)
    LoanAmount = models.IntegerField(blank=True, null=True)
    LoanAmountDate = models.DateTimeField(blank=True, null=True)
    FundName = models.CharField(max_length=40, blank=True, null=True)
    PrimaryAVS_LE = models.CharField(max_length=3, blank=True, null=True)
    PrimaryAVS_Date = models.DateTimeField(blank=True, null=True)
    PrimaryAVS_Rating = models.CharField(max_length=5, blank=True, null=True)
    Primary21st_LE = models.CharField(max_length=3, blank=True, null=True)
    Primary21st_LEMean = models.IntegerField(blank=True, null=True)
    Primary21st_Date = models.DateTimeField(blank=True, null=True)
    Primary21st_Rating = models.CharField(max_length=5, blank=True, null=True)
    SecondaryAVS_LE = models.CharField(max_length=3, blank=True, null=True)
    SecondaryAVS_Date = models.DateTimeField(blank=True, null=True)
    SecondaryAVS_Rating = models.CharField(max_length=5, blank=True, null=True)
    Secondary21st_LE = models.CharField(max_length=3, blank=True, null=True)
    Secondary21st_LEMean = models.IntegerField(blank=True, null=True)
    Secondary21st_Date = models.DateTimeField(blank=True, null=True)
    Secondary21st_Rating = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEAP2_MonthlyValuations'

class LEAP2_PremiumInfo(caching.base.CachingMixin, models.Model):
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    Year_Month = models.CharField(db_column='Year-Month', max_length=7, blank=True, null=True)
    DueDate = models.DateTimeField(blank=True, null=True)
    PremiumDue = models.IntegerField(blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    Payee = models.CharField(max_length=100, blank=True, null=True)
    PremiumReceivedDate = models.DateTimeField(blank=True, null=True)
    PremiumReceived = models.IntegerField(blank=True, null=True)
    AV = models.IntegerField(blank=True, null=True)
    DateOfAV = models.DateTimeField(blank=True, null=True)
    CSV = models.IntegerField(blank=True, null=True)
    DateOfCSV = models.DateTimeField(blank=True, null=True)
    InGrace = models.CharField(max_length=3, blank=True, null=True)
    PremConfMethod = models.CharField(max_length=25, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    PayeeID = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    InsCoID = models.IntegerField(blank=True, null=True)
    ProductID = models.IntegerField(blank=True, null=True)
    InsuredID_Primary = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PremiumID = models.IntegerField(blank=True, null=True, primary_key=True)
    Comments = models.TextField(max_length=2147483647, blank=True, null=True)
    LoanPaymentAmount = models.IntegerField(blank=True, null=True)
    LoanAmount = models.IntegerField(blank=True, null=True)
    TotalDue = models.IntegerField(blank=True, null=True)
    LoanPaymentYN = models.IntegerField(blank=True, null=True)
    LoanAmountDate = models.DateTimeField(blank=True, null=True)


    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_PremiumInfo'

class LEAP2_PremiumInfoPending(caching.base.CachingMixin, models.Model):
    SubFundName = models.CharField(max_length=50, blank=True, null=True)
    SubFundShortName = models.CharField(max_length=15, blank=True, null=True)
    Year_Month = models.CharField(db_column='Year-Month', max_length=7, blank=True, null=True)
    DueDate = models.DateTimeField(blank=True, null=True)
    PremiumDue = models.IntegerField(blank=True, null=True)
    FullNameLastFirst = models.CharField(max_length=70, blank=True, null=True)
    PolicyNumber = models.CharField(max_length=16, blank=True, null=True)
    FunderPolicyID = models.CharField(max_length=30, blank=True, null=True)
    InsuranceCompanyName = models.CharField(max_length=100, blank=True, null=True)
    PolicyStatus = models.CharField(max_length=255, blank=True, null=True)
    Payee = models.CharField(max_length=1, blank=True, null=True)
    SubFundID = models.IntegerField(blank=True, null=True)
    FundID = models.IntegerField(blank=True, null=True)
    PremiumDetailID = models.IntegerField(blank=True, null=True, primary_key=True)
    CreatedDate = models.DateTimeField(blank=True, null=True)
    Description = models.CharField(max_length=100, blank=True, null=True)
    LoanAmtDue = models.IntegerField(blank=True, null=True)
    TotalDue = models.IntegerField(blank=True, null=True)
    PolicyID = models.IntegerField(blank=True, null=True)
    objects = caching.base.CachingManager()

    class Meta:
        managed = False
        db_table = 'LEAP2_PremiumInfoPending'
