from django.db import models
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
import caching.base

class ReadAlerts(caching.base.CachingMixin, models.Model):
    user = models.ForeignKey(User)
    #SubFundID = models.ForeignKey(LEAP_SubFund, null=True, blank=True, db_column = 'objectID', to_field='objectID')
    AlertID = models.IntegerField(blank=True, null=True)
    objects = caching.base.CachingManager()
    def __unicode__(self):
        #return str(self.SubFundID)
        return "AlertID - %s - %s" % (self.user, self.AlertID)

    class Meta:
        managed = True
        db_table = 'user_alerts'
        verbose_name = 'Read Alert'
        verbose_name_plural = 'Read Alerts'

class UserSubFunds(caching.base.CachingMixin, models.Model):
    user = models.ForeignKey(User)
    #SubFundID = models.ForeignKey(LEAP_SubFund, null=True, blank=True, db_column = 'objectID', to_field='objectID')
    SubFundID = models.IntegerField(blank=True, null=True)
    objects = caching.base.CachingManager()

    def __unicode__(self):
        #return str(self.SubFundID)
        return "SubFundID"

    class Meta:
        managed = True
        db_table = 'user_sub_funds'
