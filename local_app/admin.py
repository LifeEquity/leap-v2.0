from django.contrib import admin
from models import UserSubFunds
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from models import ReadAlerts

class UserSubFundsInline(admin.TabularInline):
    model = UserSubFunds

admin.site.unregister(User)
admin.site.register(ReadAlerts)
class UserAdmin(UserAdmin):
    filter_horizontal = ('user_permissions', 'groups')
    inlines = [UserSubFundsInline]
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'last_login')
    model = User
    #def __init__(self, *args, **kwargs):
    #    super(UserAdmin, self).__init__(*args, **kwargs)
admin.site.register(User, UserAdmin)
