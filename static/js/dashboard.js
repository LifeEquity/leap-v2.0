$(document).ready(function() {

    $("#insurance_company_distribution_table").tablesorter();
    build_report_by_age_sex(false);
    build_premiums_chart(false);
    $("#alerts_list").tablesorter();
    function set_selected_fund(){
        $.ajax({
            type:'POST',
            url: '/leap/leap_set_session/',
            data: {'funds': $("#fund_multi_select").val()},
            async: true,
            success: function(data) {
                self.location.reload();
                //ajax_refresh();
            }
        });
        /*$.post(
            "/leap/leap_set_session/",
            {'funds': $("#fund_multi_select").val()},
            function(){
                //self.location.reload();
                ajax_refresh()
                // Here we can do whatever is necessary
            }

        );*/


    }
    function ajax_refresh(){
        build_report_by_age_sex(true);
        build_premiums_chart(true);
    }
    $("#fund_multi_select").multiselect({
        selectedList: 4, // 0-based index
        noneSelectedText: 'Select Funds to Limit By',
        close: function(event, ui){
            set_selected_fund();

        }

    });
    $("#alerts_list tr:last").css("border-bottom", "1px solid #7C7C7C");
    $("#portfolio_summary tr:last").css("border-bottom", "1px solid #7C7C7C");

});

function build_report_by_age_sex(load_json){
    if(load_json){
        $.get('/leap/le_report_by_age_sex/', function(data) {
            jsonData = $.parseJSON(data);
            display_le_in_months_graphs(jsonData);
        });
    } else {
            display_le_in_months_graphs(report_by_age_sex_data);
    }
}

function build_premiums_chart(load_json){
    /*$.ajax({
        type:'GET',
        url: '/leap/premiums_chart/',
        async: true,
        success: function(data) {
            jsonData = $.parseJSON(data);
            display_premiums_chart(jsonData);
        }
    });*/
    if(load_json){
        $.get('/leap/premiums_chart/', function(data) {
            jsonData = $.parseJSON(data);
            display_premiums_chart(jsonData);
        });
    } else {
            display_premiums_chart(premiums_chart_data);
    }
}
function display_le_in_months_graphs(jsonData){
    //$("#aging_le_in_months").wijbarchart('redraw', false);
    $("#aging_le_in_months").wijbarchart({
                    horizontal: false,
                    seriesStyles: [
                        {fill: "rgb(0,0,255)", stroke: "none"},
                        {fill: "rgb(255,0,0)", stroke:"none"}
                    ],
                    axis: {
                        y: {
                            alignment: "right",
                            textAlign: "right"
                        },
                        x: {
                            text: "",
                            align: "right"
                        }
                    },
                    hint: {
                        content: function () {
                            return this.data.y + " - " + this.data.label;
                        }
                    },
                    header: {
                        text: ""
                    },
                    //chartLabelFormatString: "c",
                    showChartLabels: false,
                    legend: {
                        visible: false,
                        text:"legend",
                        compass:"south",
                        orientation:"vertical",
                        style:{
                            fill:"#f1f1f1",
                            stroke:"#010101"
                        }
                    },
                    seriesList: jsonData,
                    click: function(e, data){
                        sex = data.label;
                        switch(sex){
                            case "Female":
                                sex = 'Female';
                                break;
                            case "Male":
                                sex = 'Male';
                                break;
                        }

                        index = data.index;
                        switch(index){
                            case 0:
                                months = 'NO LE Reports';
                                break;
                            case 1:
                                months = '48%2B';
                                break;
                            case 2:
                                months = '36-47';
                                break;
                            case 3:
                                months = '24-35';
                                break;
                            case 4:
                                months = '12-23';
                                break;
                            case 5:
                                months = '0-12';
                                break;
                            default:
                                months = 'blah';
                                break;

                        }
                        self.location = "/leap/insured_list/?policy_age=" + months + "&policy_gender=" + sex;

                    }
                });
    }
function display_premiums_chart(jsonData){
    $("#premiums_chart").wijbarchart({
        stacked: false,
        dataSource: jsonData,
        seriesList: [{
            dataSource: jsonData.premiums,
            label: "Premiums",
            legendEntry: true,
            data: { x: { bind: "date" }, y: { bind: "value"} }
        }],
                    horizontal: false,
                    seriesStyles: [
                        {fill: "#33D633", stroke:"none"}
                    ],
                    axis: {
                        y: {
                            annoFormatString: 'c0',
                            alignment: "right",
                            textAlign: "right",
                            autoMax: true,
                            autoMin: false
                        },
                        x: {
                            text: "",
                            align: "right"
                        }
                    },
                    hint: {
                        content: function () {
                            //return this.data.label + '\n asfd' + this.y + '';
                            duration = this.data.x;
                            return formatCurrency(this.data.y).replace('.00','');
                        }
                    },
                    header: {
                        text: ""
                    },
                    //chartLabelFormatString: "c",
                    chartLabelFormatString: "c0",
                    showChartLabels: false,
                    legend: {
                        visible: false,
                        text:"Total",
                        compass:"east",
                        orientation:"vertical",
                        style:{
                            fill:"#f1f1f1",
                            stroke:"#010101"
                        }
                    },
                    click: function(e, data){
                        month = data.x;
                        // Eventually we'll feed this to a page that will list the month i'm guessing
                        //
                        self.location = "/leap/premium_list/pending/?month=" + month;

                    }
    });
}

