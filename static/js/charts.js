$(document).ready(function() {

    $("#insurance_company_distribution_table").tablesorter();
    insurance_co_distribution_graph(false);
    build_remaining_le_distribution_chart(false);
    build_gender_age_breakdown_chart(false);
    $("#gender_age_breakdown_table").tablesorter();
    $("#alerts_list").tablesorter();
    function set_selected_fund(){
        $.ajax({
            type:'POST',
            url: '/leap/leap_set_session/',
            data: {'funds': $("#fund_multi_select").val()},
            async: true,
            success: function(data) {
                self.location.reload();
            }
        });


    }
    $("#fund_multi_select").multiselect({
        selectedList: 4, // 0-based index
        noneSelectedText: 'Select Funds to Limit By',
        close: function(event, ui){
            set_selected_fund();

        }

    });

});

function build_report_by_age_sex(load_json){
    /*$.ajax({
        type:'GET',
        url: '/leap/le_report_by_age_sex/',
        async: true,
        success: function(data) {
            jsonData = $.parseJSON(data);
            display_remaining_le_distribution_chart(jsonData);
        }
    });*/
    if(load_json){
        $.get('/leap/le_report_by_age_sex/', function(data) {
            jsonData = $.parseJSON(data);
            display_le_in_months_graphs(jsonData);
        });
    } else {
            display_le_in_months_graphs(report_by_age_sex_data);
    }
}

function build_remaining_le_distribution_chart(load_json){
    /*$.ajax({
        type:'GET',
        url: '/leap/remaining_le_distribution_chart/',
        async: true,
        success: function(data) {
            jsonData = $.parseJSON(data);
            display_le_in_months_graphs(jsonData);
        }
    });*/
    if(load_json){
        $.get('/leap/remaining_le_distribution_chart/', function(data) {
            jsonData = $.parseJSON(data);
            display_remaining_le_distribution_chart(jsonData);
        });
    } else {
            display_remaining_le_distribution_chart(remaining_le_distribution_chart_data);
    }
}
function display_le_in_months_graphs(jsonData){
    //$("#aging_le_in_months").wijbarchart('redraw', false);
    $("#aging_le_in_months").wijbarchart({
                    horizontal: false,
                    seriesStyles: [
                        {fill: "rgb(0,0,255)", stroke: "none"},
                        {fill: "rgb(255,0,0)", stroke:"none"}
                    ],
                    axis: {
                        y: {
                            alignment: "right",
                            textAlign: "right"
                        },
                        x: {
                            text: "",
                            align: "right"
                        }
                    },
                    hint: {
                        content: function () {
                            return this.data.y + " - " + this.data.label;
                        }
                    },
                    header: {
                        text: ""
                    },
                    //chartLabelFormatString: "c",
                    showChartLabels: false,
                    legend: {
                        visible: false,
                        text:"legend",
                        compass:"south",
                        orientation:"vertical",
                        style:{
                            fill:"#f1f1f1",
                            stroke:"#010101"
                        }
                    },
                    seriesList: jsonData,
                    click: function(e, data){
                        sex = data.label;
                        switch(sex){
                            case "Female":
                                sex = 'Female';
                                break;
                            case "Male":
                                sex = 'Male';
                                break;
                        }

                        index = data.index;
                        switch(index){
                            case 0:
                                months = 'NO LE Reports';
                                break;
                            case 1:
                                months = '48%2B';
                                break;
                            case 2:
                                months = '36-47';
                                break;
                            case 3:
                                months = '24-35';
                                break;
                            case 4:
                                months = '12-23';
                                break;
                            case 5:
                                months = '0-12';
                                break;
                            default:
                                months = 'blah';
                                break;

                        }
                        self.location = "/leap/insured_list/?policy_age=" + months + "&policy_gender=" + sex;

                    }
                });
    }
function display_remaining_le_distribution_chart(jsonData){
    $("#remaining_le_distribution").wijbarchart({
    dataSource: jsonData,
    stacked: true,
seriesList: [{
        dataSource: jsonData.male,
        label: "Male",
        legendEntry: true,
        data: { x: { bind: "date" }, y: { bind: "value"} }
    }, {
        dataSource: jsonData.female,
        label: "Female",
        legendEntry: true,
        data: { x: { bind: "date" }, y: { bind: "value"} }
    }],

                    horizontal: false,
                    seriesStyles: [
                        {fill: "rgb(0,0,255)", stroke: "none"},
                        {fill: "rgb(255,0,0)", stroke:"none"}
                    ],
                    /*axis: {
                        y: [{
                            text: "Total Sales",
                            annoFormatString: 'c0',
                            compass: "west"
                        },{
                            text: "Something Else",
                            compass: "east"
                        }],
                        x: {
                            text: "",
                            align: "right"
                        }
                    },*/
                    axis: {
                        y: {
                            annoFormatString: 'c0',
                            compass: "west"
                        },
                        x: {
                            text: "",
                            align: "right"
                        }
                    },
                    hint: {
                        content: function () {
                            //return this.data.label + '\n asfd' + this.y + '';
                            duration = this.data.x;
                            return this.data.label + " - " + this.data.dataSource[this.data.index].count + " - " + formatCurrency(this.data.y);
                        }
                    },
                    header: {
                        text: ""
                    },
                    chartLabelFormatString: "c0",
                    showChartLabels: false,
                    legend: {
                        visible: false,
                        compass:"east",
                        orientation:"vertical",
                        style:{
                            fill:"#f1f1f1",
                            stroke:"#010101"
                        }
                    },
                    click: function(e, data){
                        duration = data.x;
                        // Eventually we'll feed this to a page that will list the month i'm guessing
                        //
                        self.location = "/leap/policy_list/?policy_age=" + duration + "&policy_gender=" + data.label;

                    }
    });
}
    function build_gender_age_breakdown_chart(){
    $("#gender_age_breakdown").wijpiechart({
                    radius: 140,
                    legend: { visible: false, compass: 'south', orientation: 'horizontal' },
                    dataSource: gender_age_breakdown_data,
                    showChartLabels: false,
                    data: {
                        label: { bind: 'label'},
                        value: { bind: 'data'},
                        color: { bind: 'color'}

                    },
                    click: function(e, data){

                        console.log(e);
                        console.log(data.label);
                    },
                    hint: {
                        Default :{
                            height: 140

                        },
                        content: function () {
                            //return this.data.label + " Total: $110,000. 25% of total";
                            return this.data.label;
                            //return this.data.label + "<p>Total DB: $1,000,000<br />Total DB: $1,000,000</p>";
                            }
                    },
                    /*header: {
                        text: "Gender Age Breakdown"
                    },*/
                    seriesStyles: age_distribution_colors
    });
    }
    function insurance_co_distribution_graph(){
    $("#insurance_company_distribution").wijpiechart({
                    radius: 140,
                    legend: { visible: false, compass: 'south', orientation: 'horizontal' },
                    dataSource: insurance_company_distribution_data,
                    showChartLabels: false,
                    data: {
                        label: { bind: 'label'},
                        value: { bind: 'data'}
                        //offset: { bind: 'Offset'},

                    },
                    click: function(e, data){

                        console.log(e);
                        console.log(data.label);
                    },
                    hint: {
                        Default :{
                            height: 140

                        },
                        content: function () {
                            //return this.data.label + " Total: $110,000. 25% of total";
                            return this.data.label;
                            //return this.data.label + "<p>Total DB: $1,000,000<br />Total DB: $1,000,000</p>";
                            }
                    },
                    /*header: {
                        text: "Distribution by Insurance CO"
                    },*/
                    seriesStyles: generated_series_styles
    });


}

