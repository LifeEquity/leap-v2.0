# Create your views here.
# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
import models
import forms

def login_view(request):
    import leap.models as leap_models
    the_count = len(leap_models.LEAP_SubFund.objects.all())
    logout(request)
    username = ''
    password = ''
    error = None
    next_page = request.GET.get('next', None)
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        next_page = request.POST.get('next', None)
        if next_page == 'None':
            next_page = None
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if not next_page:
                    return HttpResponseRedirect(reverse('leap_index'))
                else:
                    return HttpResponseRedirect(next_page)

            else:
                return render_to_response('login/login.html', {'username': username, 'next': next_page, 'password': password, 'error':'invalid_user'}, RequestContext(request) )
                # Return a 'disabled account' error message
        else:
            return render_to_response('login/login.html', {'username': username, 'next': next_page, 'password': password, 'error':'invalid_user'}, RequestContext(request) )
    else:
        if request.GET.get('expired'):
            error = 'expired'
        else:
            error = ''
        return render_to_response('login/login.html', {'error' : error, 'next': next_page, 'username': username, 'password': password, 'list':list}, RequestContext(request) )
        # Return an 'invalid login' error message.
